"""
Amplification Models for ESRM20 based on Slope/Geology

For full details of the model refer to

Weatherill G, Crowley H, Roullé A, Tourlière B, Lemoine A, Hidalgo, C,
Kotha S R, Cotton F (2022) Modelling Site Response at Regional Scale for the
2020 European Seismic Risk Model (ESRM20), Bulletin of Earthquake Engineering,
(under review)
"""
import os
import numpy as np
import rasterio


def coefficients_to_dictionary(coeffs):
    """
    Translate the coefficients table from a string to a dictionary ordered by
    intensity measure type
    """
    output = {}
    keys = []
    for i, row in enumerate(coeffs.split('\n')):
        if not row:
            continue
        if not keys:
            keys = row.split()
            for key in keys:
                output[key] = []
        else:
            for j, val in enumerate(row.split()):
                if val not in ("pgv", "pga"):
                    output[keys[j]].append(float(val))
                else:
                    output[keys[j]].append(val)
    for key in keys:
        if key not in "imt":
            output[key] = np.array(output[key])
    return output


def get_coefficients_for_imt(coeffs, imt):
    """
    Retrieves the coefficients specific to a given IMT
    """
    # Get coefficients for imt
    if imt in ("pgv", "pga"):
        idx = coeffs["imt"].index(imt)
        C = {}
        for key in coeffs:
            C[key] = coeffs[key][idx]
    elif isinstance(imt, float):
        sa_coeffs = {}
        for key in coeffs:
            if key == "imt":
                sa_coeffs[key] = np.array(coeffs[key][2:])
            else:
                sa_coeffs[key] = coeffs[key][2:]
        if (imt < sa_coeffs['imt'][0]) | (imt > sa_coeffs["imt"][-1]):
            # Interpolation is allowed, but not extrapolation - raise error
            raise ValueError("Period %s outside supported period range (%s to %s)" %
                             str(imt), str(sa_coeffs["imt"][0]),
                             str(sa_coeffs["imt"][-1]))
        sa_coeffs_imt = np.log(sa_coeffs["imt"])
        C = {}
        for key in sa_coeffs:
            C[key] = np.interp(np.log(imt), sa_coeffs_imt, sa_coeffs[key])
    else:
        raise ValueError("IMT %s not recognised" % str(imt))
    return C


GEOLOGICAL_UNITS = ["CENOZOIC", "HOLOCENE", "JURASSIC-TRIASSIC",
                    "CRETACEOUS", "PALEOZOIC", "PLEISTOCENE",
                    "PRECAMBRIAN", "UNKNOWN"]

COEFFS_FIXED = coefficients_to_dictionary("""\
    imt               V1            V2      phi_s2s
    pgv      -0.32011591   -0.10821634   0.43790400
    pga      -0.21384649   -0.07866419   0.50748976
    0.0100   -0.21204938   -0.07786954   0.50767629
    0.0250   -0.21022718   -0.07807579   0.51225982
    0.0400   -0.20136301   -0.07541908   0.52670786
    0.0500   -0.18838689   -0.07108179   0.54493364
    0.0700   -0.17184538   -0.06571920   0.55944471
    0.1000   -0.16163003   -0.06208429   0.56625853
    0.1500   -0.16374630   -0.06294572   0.56162087
    0.2000   -0.18251959   -0.06818065   0.54476261
    0.2500   -0.21409731   -0.07647401   0.52514338
    0.3000   -0.24386901   -0.08391627   0.50768598
    0.3500   -0.27400911   -0.09230534   0.49756674
    0.4000   -0.30308465   -0.09970264   0.49551267
    0.4500   -0.33071023   -0.10723760   0.49886138
    0.5000   -0.35922788   -0.11481966   0.50191789
    0.6000   -0.38424135   -0.12166326   0.50383885
    0.7000   -0.39238013   -0.12282731   0.50457268
    0.7500   -0.39666705   -0.12348753   0.50522559
    0.8000   -0.39392763   -0.12143042   0.50479578
    0.9000   -0.39358294   -0.12038202   0.50477401
    1.0000   -0.40028453   -0.12131011   0.50481908
    1.2000   -0.40988732   -0.12295578   0.50573308
    1.4000   -0.41409963   -0.12225306   0.50983436
    1.6000   -0.42464935   -0.12508523   0.51411562
    1.8000   -0.42930176   -0.12713783   0.51401210
    2.0000   -0.42551341   -0.12617452   0.50714903
    2.5000   -0.41410716   -0.12316018   0.49538482
    3.0000   -0.39322101   -0.11745721   0.48285566
    3.5000   -0.37377357   -0.11156417   0.47392688
    4.0000   -0.35892700   -0.10678276   0.46498100
    4.5000   -0.34593942   -0.10289679   0.45244339
    5.0000   -0.32936086   -0.09799475   0.43779077
    6.0000   -0.31083191   -0.09367866   0.41948366
    7.0000   -0.29038396   -0.08769334   0.40349430
    8.0000   -0.26798948   -0.07870836   0.39340689
    """)

COEFFS_RANDOM_INT = coefficients_to_dictionary("""\
    imt       PRECAMBRIAN     PALEOZOIC   JURASSIC-TRIASSIC    CRETACEOUS      CENOZOIC   PLEISTOCENE      HOLOCENE       UNKNOWN
    pgv        0.00750400   -0.00515204         -0.06153554   -0.07234031    0.00736992    0.07121264    0.09269472   -0.03724508
    pga        0.04246887   -0.04689335         -0.12550972   -0.09220607   -0.12856371    0.17767008    0.03348558    0.14349446
    0.0100     0.04467747   -0.04719901         -0.12081014   -0.09275987   -0.12809141    0.17716640    0.03412759    0.13726836
    0.0250     0.03889165   -0.03778649         -0.12589811   -0.09016890   -0.13626584    0.17014868    0.02134094    0.16217912
    0.0400     0.03635013   -0.03184964         -0.12934774   -0.08599260   -0.14795654    0.16674580    0.00796619    0.18513009
    0.0500     0.03822106   -0.02889265         -0.13253481   -0.08320332   -0.15923731    0.16673471   -0.00399086    0.20306016
    0.0700     0.04842870   -0.02905574         -0.14094162   -0.08691977   -0.16757093    0.17089787   -0.01750887    0.22231487
    0.1000     0.06479680   -0.02666152         -0.14538543   -0.09881407   -0.15653309    0.17427644   -0.00898158    0.19655244
    0.1500     0.07592359   -0.03449021         -0.15017012   -0.11171407   -0.15175902    0.18843894    0.00585402    0.17659703
    0.2000     0.06707266   -0.03684141         -0.13591923   -0.10829435   -0.12956987    0.18685906    0.02395110    0.13564373
    0.2500     0.04549958   -0.03952544         -0.11954550   -0.09789634   -0.09538165    0.18260210    0.04789909    0.08864124
    0.3000     0.02309429   -0.04882340         -0.11020150   -0.08252276   -0.07858957    0.18895661    0.04729437    0.08229878
    0.3500    -0.00088865   -0.06080297         -0.11913566   -0.08032219   -0.04845977    0.19164678    0.08244406    0.05906910
    0.4000    -0.01394235   -0.06808797         -0.12746882   -0.07669384   -0.02102129    0.19645651    0.10471131    0.03662866
    0.4500    -0.01906609   -0.07042464         -0.12110002   -0.07169866   -0.00983259    0.18151311    0.11431121    0.02414359
    0.5000    -0.03318464   -0.06324744         -0.09788143   -0.06556113    0.01225172    0.15414730    0.12243590   -0.00543195
    0.6000    -0.05314149   -0.06436228         -0.08251682   -0.06918568    0.03294078    0.14637439    0.13892579   -0.02912185
    0.7000    -0.06083543   -0.06057868         -0.07101886   -0.06961153    0.03534846    0.13592405    0.12293154   -0.01958169
    0.7500    -0.07740521   -0.06314197         -0.07585198   -0.08188938    0.04827896    0.13735651    0.15060589   -0.03505339
    0.8000    -0.08428220   -0.06419910         -0.07628445   -0.08388821    0.04721248    0.14534687    0.14702926   -0.02542895
    0.9000    -0.08850823   -0.06219820         -0.07133046   -0.08391446    0.04406568    0.14264248    0.14560420   -0.02087862
    1.0000    -0.08695550   -0.05738604         -0.06232390   -0.07771771    0.03851214    0.13291669    0.13551423   -0.01697581
    1.2000    -0.08279930   -0.05351583         -0.05407772   -0.06869238    0.03347951    0.12221589    0.12328935   -0.01431109
    1.4000    -0.05949173   -0.03635251         -0.03538338   -0.04669483    0.02212916    0.08304733    0.08247065   -0.00669157
    1.6000    -0.06113887   -0.03333011         -0.03515454   -0.04035535    0.01985126    0.07993816    0.07719776   -0.00437551
    1.8000    -0.05820670   -0.02944153         -0.03437685   -0.03687461    0.01776400    0.07504155    0.07187361   -0.00344321
    2.0000    -0.05511928   -0.03048829         -0.03394715   -0.03445489    0.01725827    0.07299828    0.06981133   -0.00381850
    2.5000    -0.05529251   -0.03317679         -0.03524865   -0.03775773    0.01589549    0.07794768    0.07737765   -0.00519144
    3.0000    -0.04578721   -0.02599141         -0.02744938   -0.03513471    0.01117042    0.06571772    0.06714184   -0.00465567
    3.5000    -0.03981912   -0.02964418         -0.03012732   -0.03891412    0.00837912    0.06854646    0.07457700   -0.00526904
    4.0000    -0.04195342   -0.03422243         -0.03649884   -0.04116601    0.00956496    0.07505321    0.08162242   -0.00456870
    4.5000    -0.04206465   -0.04256136         -0.04317528   -0.04860474    0.01084912    0.08895943    0.09449063   -0.00778319
    5.0000    -0.04307842   -0.05127801         -0.05181793   -0.06233230    0.01139912    0.11137228    0.11654077   -0.01560194
    6.0000    -0.05288405   -0.06153656         -0.06086908   -0.08447680    0.01226796    0.14461811    0.15121486   -0.02571369
    7.0000    -0.05313788   -0.05972963         -0.05698048   -0.08639775    0.01139303    0.14354253    0.15094746   -0.02675383
    8.0000    -0.03321449   -0.03672124         -0.03072781   -0.04927415    0.00752873    0.07812242    0.08445355   -0.01061799
    """)

COEFFS_RANDOM_GRAD = coefficients_to_dictionary("""\
    imt       PRECAMBRIAN     PALEOZOIC    JURASSIC-TRIASSIC    CRETACEOUS      CENOZOIC    PLEISTOCENE      HOLOCENE      UNKNOWN
    pgv       -0.00151889    0.00104283           0.01245549    0.01464249   -0.00149176    -0.01441424   -0.01876246   0.00753883
    pga        0.01074547   -0.01778485          -0.02616742   -0.02035435   -0.04046745     0.04524993   -0.00317088   0.05341858
    0.0100     0.01110581   -0.01817585          -0.02362103   -0.01978543   -0.04021423     0.04414265   -0.00393577   0.05210902
    0.0250     0.01054942   -0.01375128          -0.03204231   -0.02329083   -0.04298207     0.04740963   -0.00096118   0.05596879
    0.0400     0.01063103   -0.01076982          -0.03745690   -0.02482504   -0.04632847     0.04969505   -0.00047448   0.05991117
    0.0500     0.01167666   -0.00905529          -0.04064013   -0.02545862   -0.04918531     0.05122655   -0.00149106   0.06298322
    0.0700     0.01522096   -0.00866859          -0.04456948   -0.02765443   -0.05138391     0.05364480   -0.00391298   0.06719302
    0.1000     0.01898016   -0.00727708          -0.04378241   -0.02975827   -0.04595861     0.05216548   -0.00146284   0.05692836
    0.1500     0.02078272   -0.01041484          -0.04151626   -0.03076920   -0.04242245     0.05227380    0.00117824   0.05081748
    0.2000     0.01679209   -0.01251480          -0.03159881   -0.02500107   -0.03540564     0.04583389    0.00087255   0.04165261
    0.2500     0.01047086   -0.01254293          -0.01977080   -0.01652711   -0.02642314     0.03637504   -0.00140843   0.03164479
    0.3000     0.00492601   -0.01305137          -0.00935782   -0.00502304   -0.02551784     0.02973268   -0.01268061   0.03384470
    0.3500     0.00068207   -0.01138237          -0.00760508   -0.00115452   -0.01955133     0.02442332   -0.01202183   0.03011394
    0.4000    -0.00007763   -0.00826703          -0.00615682    0.00128058   -0.01483467     0.02048270   -0.01209326   0.02192539
    0.4500     0.00259298   -0.00548582          -0.00129611    0.00457027   -0.01408782     0.01079136   -0.01568248   0.01890988
    0.5000     0.00454355    0.00005525           0.00659854    0.00875027   -0.00980836    -0.00351454   -0.01917515   0.01134711
    0.6000     0.00604685    0.00440535           0.01064001    0.00948379   -0.00612234    -0.01236742   -0.01943292   0.00551060
    0.7000     0.01095305    0.00931412           0.01504193    0.01493469   -0.00924357    -0.02220648   -0.02871351   0.00826076
    0.7500     0.01369981    0.01235745           0.01395140    0.01427521   -0.00674976    -0.02722199   -0.02425577   0.00311240
    0.8000     0.01694348    0.01219280           0.01497930    0.01689579   -0.00946095    -0.02828030   -0.02848641   0.00440800
    0.9000     0.01892074    0.01405716           0.01640228    0.01933592   -0.01002078    -0.03204632   -0.03297697   0.00504239
    1.0000     0.02512104    0.01688438           0.01837402    0.02268965   -0.01141742    -0.03884472   -0.03943641   0.00505860
    1.2000     0.03327967    0.01980375           0.01991772    0.02534680   -0.01264104    -0.04622581   -0.04560074   0.00437189
    1.4000     0.04722160    0.02594108           0.02737100    0.03392452   -0.01676537    -0.06227400   -0.06074314   0.00371247
    1.6000     0.05194873    0.02697120           0.02973620    0.03495830   -0.01732309    -0.06667116   -0.06444819   0.00317279
    1.8000     0.05328046    0.02727815           0.03128253    0.03528866   -0.01722153    -0.06854742   -0.06616442   0.00307244
    2.0000     0.05210980    0.02951740           0.03229472    0.03437065   -0.01640282    -0.06939661   -0.06746935   0.00295572
    2.5000     0.04758107    0.02930987           0.03042051    0.03195468   -0.01450440    -0.06586126   -0.06512613   0.00334992
    3.0000     0.04481012    0.02796003           0.02876396    0.03433113   -0.01191492    -0.06543141   -0.06637460   0.00371933
    3.5000     0.03960965    0.02549857           0.02640242    0.03376046   -0.00933018    -0.06073490   -0.06386255   0.00364497
    4.0000     0.03061486    0.02435131           0.02643817    0.03285260   -0.00685464    -0.05646726   -0.06036334   0.00381148
    4.5000     0.02253163    0.02199085           0.02451190    0.02935520   -0.00551367    -0.04940159   -0.05254284   0.00387614
    5.0000     0.01675139    0.01898142           0.02123988    0.02293046   -0.00517242    -0.04022363   -0.04159239   0.00326963
    6.0000     0.00884584    0.01248789           0.01338540    0.01433546   -0.00328602    -0.02576612   -0.02615088   0.00321300
    7.0000     0.00754320    0.01081496           0.00952506    0.01292858   -0.00247815    -0.02210686   -0.02284293   0.00365737
    8.0000     0.01794388    0.01906271           0.01478150    0.02515701   -0.00401437    -0.03950798   -0.04270189   0.00480742
    """)


def site_amp_model_slope_geology(imts, slope, geology, reference_slope,
                                 reference_geology, skip_unknown=True):
    """
    Implements the amplification function to return the amplification with
    respect to a specified reference site condition

    Args:
        imts: List of intensity measure types ['pga', 'pgv', 0.01, 0.2, ...] with periods
              given by simple floats
        slope: 2D numpy array of slope values
        geology: 2D numpy array of geology values (as numpy byte strings)
        reference_slope: Value for the reference slope condition
        reference_geology: Reference geological unit
        skip_unknown: Choose to skip amplification of locations of "UNKNOWN" geology. This
                      will return NaNs for these values, which can be useful for excluding
                      offshore sites

    Returns:
        amp: 3D grid of amplification [num. lat, num. lon., num. imts]
        phis2s: Vector of site-to-site standard deviations for each period
    """
    assert reference_geology in GEOLOGICAL_UNITS,\
        "Reference geology must be one of the supported geological units (or 'UNKNOWN')"
    nlon, nlat = slope.shape
    # Limit slope to range 0.0005 - 0.3
    islope = np.clip(slope, 0.0005, 0.3)
    amp = np.zeros([nlon, nlat, len(imts)])
    v1 = np.zeros_like(amp)
    v2 = np.zeros_like(amp)
    v1ref = np.zeros(len(imts))
    v2ref = np.zeros(len(imts))
    phi_s2s = np.zeros(len(imts))
    for i, imt in enumerate(imts):
        C = get_coefficients_for_imt(COEFFS_FIXED, imt)
        C_INT = get_coefficients_for_imt(COEFFS_RANDOM_INT, imt)
        C_GRAD = get_coefficients_for_imt(COEFFS_RANDOM_GRAD, imt)
        v1[..., i] = C["V1"]
        v2[..., i] = C["V2"]
        phi_s2s[i] = C["phi_s2s"]
        v1ref[i] = C["V1"] + C_INT[reference_geology]
        v2ref[i] = C["V2"] + C_GRAD[reference_geology]
    for geol in np.unique(geology):
        if geol.decode() not in GEOLOGICAL_UNITS:
            # Not a recognised geology - so just use the fixed effects
            continue
        idx = geology == geol
        for i, imt in enumerate(imts):
            if skip_unknown and (geol.decode() == "UNKNOWN"):
                # Setting unknown sites to nan - used for offshore and/or
                # unclassified geology
                v1[idx, i] = np.nan
                v2[idx, i] = np.nan
            else:
                # Add the random effect
                C_INT = get_coefficients_for_imt(COEFFS_RANDOM_INT, imt)
                C_GRAD = get_coefficients_for_imt(COEFFS_RANDOM_GRAD, imt)
                v1[idx, i] += C_INT[geol.decode()]
                v2[idx, i] += C_GRAD[geol.decode()]
    for i in range(len(imts)):
        amp[..., i] = np.exp(v1[..., i] + v2[..., i] * np.log(islope)) /\
            np.exp(v1ref[i] + v2ref[i] * np.log(reference_slope))
    return amp, phi_s2s


def export_amplification_maps_to_geotiff(output_folder, amplification, imts, bbox,
                                         spcx, spcy, nodata=-99.):
    """
    Exports the amplification maps to geotiff using rasterio

    Args:
        output_folder: Path to the folder for exporting the geotif files of the
                       amplification map
        amplification: Amplification data as numpy array
        imts: List of intensity measure types
        bbox: Bounding box as a tuple of (llon, llat, ulon, ulat)
        spcx: Longitude spacing
        spcy: Latitude spacing
        nodata: Value to use for nodata values
    """
    llon, llat, ulon, ulat = tuple(bbox)
    transform = (
        rasterio.transform.Affine.translation(
            llon,
            llat,
        )
        * rasterio.transform.Affine.scale(spcx, spcy)
    )
    for i, imt in enumerate(imts):
        fname = os.path.join(output_folder,
                             "amplification_{:s}.tif".format(str(imt)))
        ampl = amplification[..., i]
        ampl[np.isnan(ampl)] = nodata
        with rasterio.open(
            fname,
            "w",
            "GTiff",
            height=ampl.shape[0],
            width=ampl.shape[1],
            count=1,
            dtype=ampl.dtype,
            crs="+proj=latlong",
            transform=transform,
            compress="lzw",
            nodata=nodata,
        ) as dst:
            dst.write(np.flipud(ampl), 1)
        print("Written amplification map for %s to %s" % (str(imt), fname))
    return


def export_amplification_maps_to_ascii(output_folder, amplification, imts, bbox,
                                       spcx, spcy, nodata=-99.0):
    """
    Args:
        output_folder: Path to the folder for exporting the geotif files of the
                       amplification map
        amplification: Amplification data as numpy array
        imts: List of intensity measure types
        bbox: Bounding box as a tuple of (llon, llat, ulon, ulat)
        spcx: Longitude spacing
        spcy: Latitude spacing
        nodata: Value to use for nodata values
    """
    nrow, ncol, nimt = amplification.shape
    llon, llat, ulon, ulat = tuple(bbox)
    for i, imt in enumerate(imts):
        fname = os.path.join(output_folder,
                             "amplification_{:s}.asc".format(str(imt)))
        ampl = amplification[..., i]
        ampl[np.isnan(ampl)] = nodata
        with open(fname, "w") as f:
            f.write("NCOLS     %g\n" % ncol)
            f.write("NROWS     %g\n" % nrow)
            f.write("XLLCENTER     %.6f\n" % (llon + spcx / 2.))
            f.write("YLLCENTER     %.6f\n" % (llat + spcx / 2.))
            f.write("CELLSIZE      %.6f\n" % spcx)
            f.write("NODATA_VALUE      %.5f\n" % nodata)
            np.savetxt(f, ampl, fmt="%.5f")
        print("Written amplification map for %s to %s" % (str(imt), fname))
    return
