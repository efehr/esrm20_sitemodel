"""
Python tools for assigning site information to different exposure configuration

Data:
1. Input slope and elevation data files at 30 arc seconds in hdf5 binaries
2. Input geology model as a shapefile

"""
import os
import warnings
import h5py
import argparse
import numpy as np
import pandas as pd
import geopandas as gpd
from scipy import stats
from scipy.interpolate import RegularGridInterpolator
from pyproj import CRS, Transformer
from shapely.geometry import Point
from exposure2site.node_handler import Node, nrml_read, nrml_write
from exposure2site.amplification_model import (
    site_amp_model_slope_geology,
    export_amplification_maps_to_geotiff,
    export_amplification_maps_to_ascii,
    GEOLOGICAL_UNITS
)

# Coordinate reference systems
CRS_WGS84 = CRS.from_epsg(4326)
MOLL_STR = '+ellps=WGS84 +lon_0=0 +no_defs +proj=moll +units=m +x_0=0 +y_0=0'
CRS_MOLL = CRS.from_string(MOLL_STR)
EUROPE_EQUAL_AREA = "epsg:3035"
SMOOTHED_N = [1, 3, 5]
# Transformations between Molleweide and WGS84
TRANSFORMER = Transformer.from_crs(CRS_WGS84, CRS_MOLL, always_xy=True)
TRANSFORMER_REV = Transformer.from_crs(CRS_MOLL, CRS_WGS84, always_xy=True)

# Geological eras stored as integers - relates the geological era to the int
GEOL_DICT_KEY_TO_NAME = {0: "UNKNOWN",
                         1: "PRECAMBRIAN",
                         2: "PALEOZOIC",
                         3: "JURASSIC-TRIASSIC",
                         4: "CRETACEOUS",
                         5: "CENOZOIC",
                         6: "PLEISTOCENE",
                         7: "HOLOCENE"}


GEOL_DICT_NAME_TO_KEY = {"UNKNOWN": 0,
                         "PRECAMBRIAN": 1,
                         "PALEOZOIC": 2,
                         "JURASSIC-TRIASSIC": 3,
                         "CRETACEOUS": 4,
                         "CENOZOIC": 5,
                         "PLEISTOCENE": 6,
                         "HOLOCENE": 7}


# Data files
DATA_PATH = os.path.join(os.path.dirname(__file__), "site_data")
# Geology shapefiles
GEOLOGY_FILE = os.path.join(DATA_PATH, "GEOL_V8_ERA2.shp")
# File containing 30 arc second grid of slope, geology, elevation and
# built density
# BBOX = [-25.0, 34.0, 45.0, 72.0]
SITE_HDF5_FILE = os.path.join(DATA_PATH, "europe_gebco_topography.hdf5")
# File containing Vs30s at 30 arcseconds using the USGS Wald & Allen method
# BBOX = [-30;, 30., 65., 75.]
VS30_HDF5_FILE = os.path.join(DATA_PATH, "europe_2020_vs30.hdf5")
# Pre-computed grid of volcanic front distances at 5 arc-minutes
# BBOX = [-25.0, 34.0, 45.0, 72.0]
VF_DIST_FILE = os.path.join(DATA_PATH, "volcanic_front_distances_5min.hdf5")
# Europe slice of 250 m x 250 m GHS built density dataset (in Mollewiede proj.)
BUILT_ENV_FILE = os.path.join(DATA_PATH, "ghs_global_250m_Europe.hdf5")
# ESHM20 Attenuation Region File
REGION_FILE = os.path.join(DATA_PATH, "ATTENUATION_REGIONS.shp")

# See if the files are present and raise warnings if not
for fname in [GEOLOGY_FILE, SITE_HDF5_FILE, VS30_HDF5_FILE, VF_DIST_FILE,
              BUILT_ENV_FILE, REGION_FILE]:
    if not os.path.exists(fname):
        warnings.warn("Required file %s not found in data directory "
                      "- if needed, please download from ..." % fname)


# =============================================================================
# ================= General utility functions =================================
# =============================================================================


def layer_to_gridfile(gx, gy, data, spc, output_file, fmt="%.3f"):
    """
    Exports a layer to an ESRI ascii raster file
    """
    odata = np.copy(data)
    odata[np.isnan(odata)] = -999.0
    with open(output_file, "w") as f:
        f.write("NCOLS %g\n" % len(gx))
        f.write("NROWS %g\n" % len(gy))
        f.write("XLLCENTER %f\n" % gx[0])
        f.write("YLLCENTER %f\n" % gy[-1])
        f.write("CELLSIZE %f\n" % spc)
        f.write("NODATA_VALUE %f\n" % -999.0)
        np.savetxt(f, odata, fmt=fmt)


def get_admin_level_definition(country_name, path, output_dfs=False):
    """
    < From Cecilia Nievas >
    This function returns the most-detailed administrative level for which the
    SERA exposure model is defined for country_name. The output is a dictionary
    with the admin level for each case of exposure (Res, Com, Ind), and a list
    grouping these three cases according to common levels of administrative
    units.

    I have tested it manually (and it has passed the test) for:
    - France ({'Res': 5, 'Com': 2, 'Ind': 5}, [['Com'], ['Res', 'Ind']], [2, 5], {dataframes_of_csv_files})
    - Germany ({'Res': 4, 'Com': 3, 'Ind': 4}, [['Com'], ['Res', 'Ind']], [3, 4], {dataframes_of_csv_files})
    - Greece ({'Res': 3, 'Com': 3, 'Ind': 3}, [['Res', 'Com', 'Ind']])
    - Italy ({'Res': 3, 'Com': 3, 'Ind': 3}, [['Res', 'Com', 'Ind']])
    - Spain ({'Res': 2, 'Com': 1, 'Ind': 2}, [['Com'], ['Res', 'Ind']])
    - United Kingdom ({'Res': 2, 'Com': 1, 'Ind': 2}, [['Com'], ['Res', 'Ind']])

    NOTE: It is not possible to break the for i in range(1, max_possible_level+1)
    loop because there are files for which intermediate levels are missing but
    more detailed levels are available.
    """
    max_possible_level = 5
    exposure_cases = ['Res', 'Com', 'Ind']
    actual_levels_list = [0, 0, 0]
    out_dataframes = {}
    for k, case in enumerate(exposure_cases):
        aux_df = pd.read_csv(os.path.join(
            path,
            'Exposure_' + case + '_' + country_name + '.csv'),
            sep=',')
        if output_dfs:
            out_dataframes[case] = aux_df
        for i in range(0, max_possible_level + 1):
            if 'id_' + str(i) in aux_df.columns:
                if np.all(aux_df['id_' + str(i)].values != 'No_tag'):
                    # if not('No_tag' in aux_df['id_'+str(i)].values):
                    actual_levels_list[k] = i
    groups = []
    groups_levels = []
    case_count = 0
    for i in range(0, max_possible_level + 1):
        which = np.where(np.array(actual_levels_list) == i)[0]
        if len(which) > 0.5:
            aux_list = []
            for pos in which:
                aux_list.append(exposure_cases[pos])
                case_count = case_count + 1
            groups.append(aux_list)
            groups_levels.append(i)
        if case_count >= len(exposure_cases):
            break
    actual_levels = {}
    actual_levels['Res'] = actual_levels_list[0]
    actual_levels['Com'] = actual_levels_list[1]
    actual_levels['Ind'] = actual_levels_list[2]
    return actual_levels, groups, groups_levels, out_dataframes


def get_maximum_admin_level(admin):
    """
    Get the maximum admin level defined in an administrative geodataframe
    """
    max_admin_level = 0
    for key in admin.columns:
        if key.startswith("ID_"):
            admin_level = int(key.lstrip("ID_"))
            if admin_level > max_admin_level:
                max_admin_level = admin_level
    return max_admin_level


def dissolve_admin(admin, adm_level):
    """
    If the desired admin level is lower than that for which the admin is
    defined (and assuming all admins are given in the file via ID_0, ID_1,
    ID_2 etc.) then dissolve the administrative regions to a lower level
    """
    if adm_level >= get_maximum_admin_level(admin):
        # No need to dissolve as the desired level is at the resolution level
        return admin
    id_str = "ID_{:g}".format(adm_level)
    name_str = "NAME_{:g}".format(adm_level)
    output_admin = admin.dissolve(id_str)
    # As dissovling turns the target colummn into the index, build a simpler
    # dataframe including all attributes at a lower admin level
    data4admin = {"geometry": output_admin["geometry"].values,
                  id_str: output_admin.index.values,
                  name_str: output_admin[name_str].values}
    for i in range(adm_level):
        lev_id = "ID_{:g}".format(i)
        lev_name = "NAME_{:g}".format(i)
        data4admin[lev_id] = output_admin[lev_id].values
        data4admin[lev_name] = output_admin[lev_name].values
    return gpd.GeoDataFrame(data4admin)


KEY_MAPPER = dict([("id_{:g}".format(g), "ID_{:g}".format(g))
                   for g in range(6)])
for i in range(6):
    KEY_MAPPER["name_{:g}".format(i)] = "NAME_{:g}".format(i)


def get_site_set_from_exposure(exposure_file,
                               shapefile_path="./Europe_shapefiles/",
                               target_admin_level=None):
    """
    The point exposure models are separated from the polygon geometries, the
    former in csv (assigning the point to the admin level) and the latter in
    shapefiles (mostly, but not always, corresponding to the same admin level).
    For a given exposure model file this finds the corresponding shapefile,
    loads in the dataframe and returns a geopandas GeoDataframe with the
    polygons as the geometry column and the lons, lats of the exposure model
    added as separate columns. This permits the spatial aggregation of the
    site model over the complete polygon whilst assigning the values to the
    desired exposure points.

    Note that duplicate points are dropped
    """
    # Retrieve country and exposure type from filename
    file_info = os.path.splitext(
        os.path.split(exposure_file)[-1])[0].split("_")
    country = file_info[-1]
    exposure_type = file_info[-2]

    # retrive country name for op to three words (e.g., Isle_of_Man)
    if len(file_info) == 4:
        country = file_info[-2] + '_' + file_info[-1]
        exposure_type = file_info[-3]
    elif len(file_info) == 5:
        country = file_info[-3] + '_' + file_info[-2] + '_' + file_info[-1]
        exposure_type = file_info[-4]
    else:
        country = file_info[-1]
        exposure_type = file_info[-2]

    # Read in the exposure file
    exposure = pd.read_csv(exposure_file, sep=",")
    # Some exposure files have ID and NAME colums in lowe case, whilst the
    # polygons have it in upper case. This transforms the admin level IDs and
    # NAME headers all to upper case
    exposure = exposure.rename(KEY_MAPPER, axis='columns')
    # Determine the highest admin level adopted within the exposure model
    admin_level = 0
    for col in exposure.columns:
        if col.upper().startswith("ID_"):
            level = int(col.split("_")[1])
            if level > admin_level:
                # Goes to a higher admin level
                admin_level = level
    # Check a coreresponsing shapefile can be found
    shp_fname = "Adm{:g}_{:s}.shp".format(admin_level, country)
    if shp_fname not in os.listdir(shapefile_path):
        raise ValueError(
            "Shapefile %s not found in path %s" % (shp_fname, shapefile_path))
    # Load in the geometry shapefiles for the admin units
    admin = gpd.GeoDataFrame.from_file(os.path.join(shapefile_path, shp_fname))
    if target_admin_level and (target_admin_level < admin_level):
        # The admin level of the exposure is lower than the admin level of the
        # shapefile - dissolve the shapefile to a lower admin level
        admin = dissolve_admin(admin, target_admin_level)
        admin.crs = {"init": "epsg:4326"}
    else:
        target_admin_level = admin_level
    # Drop the duplicate sites in the exposure model to retain only one
    # location per admin region
    exposure_clean = exposure.drop_duplicates(["lon", "lat"])

    # Now check that every site in the exposure has a corresponding
    # polygon in the admin file and if so add the exposure site lon and lat
    # to the shapefile
    # change type of id-column to string consistent with admin
    admin_id = "ID_{:g}".format(target_admin_level)
    if type(admin[admin_id].iloc[0]) == str:
        exposure_clean[admin_id] = exposure_clean[admin_id].astype(str)
    # discard lead zeros (i.e., ID_1= 0111)
    # csv exposure discards leaded by zero
    for ii in range(len(admin)):
        if str(admin[admin_id].loc[ii])[0] == '0':
            admin.loc[ii, admin_id] = admin[admin_id].loc[ii][1:]

    # exclude geometries missing from exposure
    admin = admin[((admin[admin_id].isin(exposure_clean[admin_id])))]
    admin.index = range(len(admin.index))

    lons = []
    lats = []
    for adm in admin[admin_id].values:
        idx = exposure_clean[admin_id].values == adm
        if np.any(idx):
            lon = exposure_clean[idx].lon.values[0]
            lat = exposure_clean[idx].lat.values[0]
            lons.append(lon)
            lats.append(lat)
        else:
            print("No geometry found for {:s} {:s}".format(admin_id, str(adm)))
            lons.append(np.nan)
            lats.append(np.nan)
    admin["lon"] = np.array(lons)
    admin["lat"] = np.array(lats)
    return admin, target_admin_level


# =============================================================================
# ======= Collection of methods for retrieving maxs, means and medians ========
# ======= from weighted data ==================================================


def weighted_mean(values, weights):
    """
    Returns the weighted mean of a vector
    """
    # Normalise the weights
    sum_weights = np.sum(weights)
    if not sum_weights:
        # No weights found in data - return normal mean
        return np.mean(values)
    weights /= sum_weights
    return np.sum(values * weights)


def weighted_median(values, weights):
    """
    Returns the weighted median of a vector
    """
    sum_weights = np.sum(weights)
    if not sum_weights:
        # No weights found in data - return normal median
        return np.percentile(values, 50)
    # Normalise the weights
    weights /= sum_weights
    # Sort the values in ascending order
    idx = np.argsort(values)
    # Cumulate the weights and values
    cum_weights = np.cumsum(weights[idx])
    values = values[idx]
    # Identify the the location of the element with the
    # weight of 0.5 or greater
    iloc = np.where(cum_weights >= 0.5)[0][0]
    return values[iloc]


def max_weight(values, weights):
    """
    Returns the highest weighted value from a vector
    """
    if not np.sum(weights):
        # No weights given - take ordinary max
        return np.max(values)
    return values[np.argmax(weights)]


WEIGHTING_METHODS = {
    "MEAN": weighted_mean,
    "MAX": max_weight,
    "MEDIAN": weighted_median,
}


UNWEIGHTED_METHODS = {
    "MEAN": lambda x: np.mean(x),
    "MEDIAN": lambda x: np.percentile(x, 50),
    "MAX": lambda x: np.max(x)
}


# =============================================================================
# === Collection of methods for retrieving weighted centroids from data =======
# =============================================================================


def weighted_centroid(xlocs, ylocs, weights):
    """
    Returns the centroid of a set of weighted points
    """
    sum_weights = np.sum(weights)
    if sum_weights:
        # Normalise the weights
        weights /= sum_weights
    else:
        # Assume evenly weighted - should produce same output as normal
        # centroid
        return np.mean(xlocs), np.mean(ylocs)
    # Get centroid
    centx = np.sum(xlocs * weights)
    centy = np.sum(ylocs * weights)
    return centx, centy


def max_weight_centroid(xlocs, ylocs, weights):
    """
    Returns the location of the point of maximum weight from a set of
    weighted points
    """
    if not np.sum(weights):
        # No weights, so just return normal centroid
        return np.mean(xlocs), np.mean(ylocs)
    amax = np.argmax(weights)
    centx = xlocs[amax]
    centy = ylocs[amax]
    return centx, centy


def weighted_medoid(xlocs, ylocs, weights):
    """
    Returns the medoid of a set of points (i.e. the location within the
    data set exceeded by half the weight)
    """
    sum_weights = np.sum(weights)
    if not sum_weights:
        # No weights, so just return normal centroid
        return np.mean(xlocs), np.mean(ylocs)
    # Normalise the weights
    weights /= np.sum(weights)
    # Re-order the weights and points from lowest weight to highest
    idx = np.argsort(weights)
    xlocs = xlocs[idx]
    ylocs = ylocs[idx]
    # Cumulate the normalised weights
    cum_w = np.cumsum(weights[idx])
    # Find the first point where the weights exceed 0.5
    iloc = np.where(cum_w >= 0.5)[0][0]
    centx = xlocs[iloc]
    centy = ylocs[iloc]
    return centx, centy


CENTROID_METHODS = {
    "MEAN": weighted_centroid,
    "MAX": max_weight_centroid,
    "MEDIAN": weighted_medoid
}


# =============================================================================
# =========== Utility methods for accessing/slicing data sets =================
# =============================================================================

def slice_wgs84_datafile(dbfile, bounds, datakey, flatten=False):
    """
    For data stored in an hdf5 data file, slice out the data set within
    a bounding box

    :param str dbfile:
        Path to the database file
    :param list bounds:
        Bounding box define as [llon, llat, ulon, ulat]
    :param str datakey:
        Name of the dataset to be sliced
    :param bool flatten:
        If True then flattens the sliced data to three 1-D vectors of lon, lat
        and data, otherwise returns the lons and lats as 1-D vectors of
        (nlon,) and (nlat,) respectively and data as a 2-D vector (nlat, nlon)
    """
    llon, llat, ulon, ulat = tuple(bounds)
    db = h5py.File(dbfile, "r")
    lons = db["longitude"][:]
    lats = db["latitude"][:]
    idx_lon = np.logical_and(lons >= llon, lons <= ulon).nonzero()[0]
    idx_lat = np.logical_and(lats >= llat, lats <= ulat).nonzero()[0]
    lons = lons[idx_lon]
    lats = lats[idx_lat]
    data = db[datakey][idx_lat, :][:, idx_lon]
    db.close()
    if flatten:
        lons, lats = np.meshgrid(lons, lats)
        return lons.flatten(), lats.flatten(), data.flatten()
    else:
        return lons, lats, data


def interpolate_xvf_grid(target_lons, target_lats, default_xvf=150.):
    """
    Defines the volcanic front distance(xvf) at a set of locations by direct 2D
    linear interpolation from the 5 arc minute grid
    :param np.ndrray target_lons:
        Vector (1D) of longitudes
    :param np.ndarray target_lats:
        Vector (1D) of latitudes
    :param float default_xvf:
        Default volcanic front distance (km)
    """

    # Define the bounding box of the region - with a 10 arc minute buffer
    spc = 1. / 6.
    bbox = (np.min(target_lons) - spc, np.min(target_lats) - spc,
            np.max(target_lons) + spc, np.max(target_lats) + spc)
    # Extract the slice of the 5' volcanic distance grid
    lons, lats, xvf = slice_wgs84_datafile(VF_DIST_FILE, bbox, "xvf")
    if np.all(np.fabs(xvf - default_xvf) < 1.0E-3):
        # No relevant volcanic distance within bounding box
        return default_xvf * np.ones(target_lons.shape)
    # Interpolate from regular grid to target sites
    lons, lats = np.meshgrid(lons, lats)
    ipl = RegularGridInterpolator((lons[0, :], lats[:, 0][::-1]),
                                  xvf[::-1, :].T, bounds_error=False,
                                  fill_value=default_xvf)
    output_xvf = ipl(np.column_stack([target_lons.flatten(),
                                      target_lats.flatten()]))
    return np.reshape(output_xvf, target_lons.shape)


# =============================================================================
# =========== Methods to downsample grids (w/ or w/o weighting) ===============
# =============================================================================

def downsample_weighted_grid(lons, lats, data, weights, num, method="MEAN"):
    """
    Downsamples a grid of weighted data returning a coarser grid with the
    cell values determined from the corresponding statistical processing
    (MEAN, MEDIAN or MAX)

    :param np.ndarray lons:
        Vector of (nlon,) longitudes of the grid cell centres
    :param np.ndarray lats:
        Vector of (nlat,) latitudes of the grid cell centres
    :param np.ndarray data:
        Grid of data to be downsampled of size (nlat, nlon)
    :param np.ndarray weights:
        Grid of weights to be applied to the data (nlat, nlon)
    :param int num:
        Number of higher resolution cells per low resolution cell
    :param str method:
        Choice of method for defining the downsampled cell values from ("MEAN",
        "MEDIAN" or "MAX")
    :returns:
        ds_data: Downsamples grid values
        gx: Downsamples longitudes (cell centres)
        gy: Downsamples latitudes (cell centres)
    """
    nlon, nlat = len(lons), len(lats)
    # Longitudes and latitudes must be the same shape as the data
    assert nlat == data.shape[0]
    assert nlon == data.shape[1]
    xloc = np.arange(0, nlon, num)
    yloc = np.arange(0, nlat, num)
    spcx = np.diff(lons)
    spcy = np.diff(lats)
    # Get centres of downsample grid
    gx = lons[xloc] - (spcx[0] / 2.) + ((num / 2.) * spcx[0])
    gy = lats[yloc] - (spcy[0] / 2.) + ((num / 2.) * spcy[0])
    ds_data = np.zeros([len(yloc), len(xloc)])
    for j in range(len(yloc)):
        if j == (len(yloc) - 1):
            rval_m = data[yloc[j]:, :]
            weights_m = weights[yloc[j]:, :]
        else:
            rval_m = data[yloc[j]:yloc[j + 1], :]
            weights_m = weights[yloc[j]:yloc[j + 1], :]
        for i in range(len(xloc)):
            if i == (len(xloc) - 1):
                cell_data = rval_m[:, xloc[i]:]
                cell_weights = weights_m[:, xloc[i]:]
            else:
                cell_data = rval_m[:, xloc[i]:xloc[i + 1]]
                cell_weights = weights_m[:, xloc[i]:xloc[i + 1]]
            # Get valid number of cells
            idx_data = np.logical_and(np.logical_not(np.isnan(cell_data)),
                                      np.logical_not(np.isinf(cell_data)))
            if not np.any(idx_data):
                # No valid data at all in cell
                ds_data[j, i] = np.nan
            cell_data = cell_data[idx_data]
            cell_weights = cell_weights[idx_data]
            if not np.any(cell_weights > 0.0):
                # All cells have a zero weight - therefore they
                # can be considered evenly weighted and only the mean should
                # be considered
                ds_data[j, i] = np.mean(cell_data)
            else:
                # Normalise the weights
                ds_data[j, i] = WEIGHTING_METHODS[method](
                    cell_data.flatten(), cell_weights.flatten())
    return ds_data, gx, gy


def downsample_grid(lons, lats, data, num, method="MEAN"):
    """
    Downsamples a grid of data returning a coarser grid with the
    cell values determined from the corresponding statistical processing
    (MEAN, MEDIAN or MAX)

    :param np.ndarray lons:
        Vector of (nlon,) longitudes of the grid cell centres
    :param np.ndarray lats:
        Vector of (nlat,) latitudes of the grid cell centres
    :param np.ndarray data:
        Grid of data to be downsampled of size (nlat, nlon)
    :param int num:
        Number of higher resolution cells per low resolution cell
    :param str method:
        Choice of method for defining the downsampled cell values from
        ("MEAN", "MEDIAN" or "MAX")
    :returns:
        ds_data: Downsamples grid values
        gx: Downsamples longitudes (cell centres)
        gy: Downsamples latitudes (cell centres)
    """
    nlon, nlat = len(lons), len(lats)
    # Longitudes and latitudes must be the same shape as the data
    assert nlat == data.shape[0]
    assert nlon == data.shape[1]
    xloc = np.arange(0, nlon, num)
    yloc = np.arange(0, nlat, num)
    spcx = np.diff(lons)
    spcy = np.diff(lats)
    # Get centres of downsample grid
    gx = lons[xloc] - (spcx[0] / 2.) + ((num / 2.) * spcx[0])
    gy = lats[yloc] - (spcy[0] / 2.) + ((num / 2.) * spcy[0])
    ds_data = np.zeros([len(yloc), len(xloc)])
    for j in range(len(yloc)):
        if j == (len(yloc) - 1):
            rval_m = data[yloc[j]:, :]
        else:
            rval_m = data[yloc[j]:yloc[j + 1], :]
        for i in range(len(xloc)):
            if i == (len(xloc) - 1):
                cell_data = rval_m[:, xloc[i]:]
            else:
                cell_data = rval_m[:, xloc[i]:xloc[i + 1]]
            # Get valid number of cells
            idx_data = np.logical_and(np.logical_not(np.isnan(cell_data)),
                                      np.logical_not(np.isinf(cell_data)))
            if not np.any(idx_data):
                # No valid data at all in cell
                ds_data[j, i] = np.nan
            else:
                cell_data = cell_data[idx_data]
                ds_data[j, i] = UNWEIGHTED_METHODS[method](cell_data)
    return ds_data, gx, gy


def downsample_modal_grid(lons, lats, data, num):
    """
    Downsamples a grid of categorical data, assigning to each grid cell the
    modal value found within the subcells

    :param np.ndarray lons:
        Vector of (nlon,) longitudes of the grid cell centres
    :param np.ndarray lats:
        Vector of (nlat,) latitudes of the grid cell centres
    :param np.ndarray data:
        Grid of data to be downsampled of size (nlat, nlon)
    :param int num:
        Number of higher resolution cells per low resolution cell
    :returns:
        ds_data: Downsampled grid values
        gx: Downsampled longitudes (cell centres)
        gy: Downsampled latitudes (cell centres)
    """
    nlon, nlat = len(lons), len(lats)
    # Longitudes and latitudes must be the same shape as the data
    assert nlat == data.shape[0]
    assert nlon == data.shape[1]
    xloc = np.arange(0, nlon, num)
    yloc = np.arange(0, nlat, num)
    spcx = np.diff(lons)
    spcy = np.diff(lats)
    gx = lons[xloc] - (spcx[0] / 2.) + ((num / 2.) * spcx[0])
    gy = lats[yloc] - (spcy[0] / 2.) + ((num / 2.) * spcy[0])
    ds_data = np.zeros([len(yloc), len(xloc)], dtype=data.dtype)
    if ("float" in data.dtype.name) or ("int" in data.dtype.name):
        # Data is float or int
        is_num = True
    else:
        # Could be a categorical string array
        is_num = False

    for j in range(len(yloc)):
        if j == (len(yloc) - 1):
            rval_m = data[yloc[j]:, :]
        else:
            rval_m = data[yloc[j]:yloc[j + 1], :]
        for i in range(len(xloc)):
            if i == (len(xloc) - 1):
                cell_data = rval_m[:, xloc[i]:]
            else:
                cell_data = rval_m[:, xloc[i]:xloc[i + 1]]
            # Get valid number of cells
            if is_num:
                idx_data = np.logical_and(np.logical_not(np.isnan(cell_data)),
                                          np.logical_not(np.isinf(cell_data)))
            else:
                idx_data = np.logical_and(np.logical_not(cell_data == "inf"),
                                          np.logical_not(cell_data == "nan"))
            if not np.any(idx_data):
                # No valid data at all in cell
                ds_data[j, i] = np.nan
            else:
                ds_data[j, i] = stats.mode(cell_data[idx_data],
                                           axis=None).mode[0]
    return ds_data, gx, gy


# =============================================================================
# ===== Methods to build slope, geology, vs30 and backarc distance grids ======
# =============================================================================


def downsample_slope_geology_vs30_grid(bbox, num, weighting, averaging="MEAN",
                                       geol_weighting="MODE", default_xvf=150.,
                                       n=0, onshore_only=True,
                                       as_dataframe=False):
    """
    Starting from the 30 arc-seconds grid, downsamples the model to a
    coarser regular grid within a bounding box with spacing a multiple of 30
    arc-seconds

    :param list bbox:
        Bounding box [llon, llat, ulon, ulat] in decimal degrees
    :param int num:
        Number of 30 arc second cells per downsampled cell (e.g. 60 arc-seconds
        = 2, 90 arc-seconds=3, 120 arc-seconds=4)
    :param bool weighting:
        Weight the averages according to density
    :param str averaging:
        Choice of method for taking the values in the downsampled cells
        ("MEAN", "MEDIAN" or "MAX")
    :param str geol_weighting:
        For the geology take either the most common unit per downsampled
        grid cell ("MODE") or sort the units by weighting and take the median
    :param float default_xvf:
        Default value of volcanic front distance for sites unaffected by
        subduction or deep earthquakes
    :param bool onshore_only:
        Limit the returned dataframe only to those sites onshore (as determined
        by elevation >= -5 m above M.S.L.
    :param bool as_dataframe:
        Return the model as a geopandas GeoDataFrame (True) or as a set of
        arrays (False)
    """
    if not (averaging in ("MEAN", "MEDIAN", "MAX")):
        raise ValueError("Averaging method %s not recognised" % averaging)
    # Get the reference longitudes, latitudes, slopes and geology grids
    print("---- Retrieving data within bounding box")
    lons, lats, slope = slice_wgs84_datafile(SITE_HDF5_FILE, bbox, "slope")
    geology = slice_wgs84_datafile(SITE_HDF5_FILE, bbox, "geology")[2]
    vs30 = slice_wgs84_datafile(VS30_HDF5_FILE, bbox, "vs30")[2]

    if weighting or (geol_weighting == "MEDIAN"):
        # Using weighting, so extract density
        density_key = "built_density"
        if n and (n in SMOOTHED_N):
            # Use the smoothed density
            density_key += "_n{:g}".format(n)

        density = slice_wgs84_datafile(SITE_HDF5_FILE, bbox, density_key)[2]
    print("---- Downsampling slope and Vs30")
    if weighting:
        # Get the slopes and vs30s weighted by the building density
        slopes, glons, glats = downsample_weighted_grid(
            lons, lats, slope, density, num, method=averaging)
        vs30s = downsample_weighted_grid(lons, lats, vs30, density, num,
                                         method=averaging)[0]
    else:
        # Get the slopes and vs30s by averaging without weighting
        slopes, glons, glats = downsample_grid(lons, lats, slope, num,
                                               method=averaging)
        vs30s = downsample_grid(lons, lats, vs30, density, num,
                                method=averaging)[0]
    # Need to add on the geology
    print("---- Downsampling geology")
    if geol_weighting in ("MEDIAN", "MAX"):
        # Get the weighted median or weighted max geological category per cell
        geologies = downsample_weighted_grid(lons, lats, geology, density,
                                             num, method=geol_weighting)[0]
    else:
        # Gets the modal geological category per cell
        geologies = downsample_modal_grid(lons, lats, geology, num)[0]
    # Need to add on backarc distances for the target sites
    print("---- Getting backarc distance")
    glons, glats = np.meshgrid(glons, glats)
    xvf = interpolate_xvf_grid(glons, glats, default_xvf)

    if onshore_only:
        print("---- Clipping to onshore")
        raw_elevation = slice_wgs84_datafile(SITE_HDF5_FILE, bbox,
                                             "elevation")[2]
        if weighting:
            elevations = downsample_weighted_grid(lons, lats, raw_elevation,
                                                  density, num,
                                                  method=averaging)[0]
        else:
            elevations = downsample_grid(lons, lats, raw_elevation,
                                         num, method=averaging)[0]
        idx = elevations >= -5.0
    else:
        idx = np.ones(glons.shape, dtype=bool)

    if as_dataframe:
        print("---- Returning dataframe")
        # Return the grids as geopandas GeoDataFrame
        glons = glons[idx].flatten()
        glats = glats[idx].flatten()
        return gpd.GeoDataFrame({
            "geometry": gpd.GeoSeries([Point(lon, lat) for lon, lat in
                                       zip(glons, glats)]),
            "lon": glons,
            "lat": glats,
            "vs30": vs30s[idx].flatten(),
            "slope": slopes[idx].flatten(),
            "geology": [GEOL_DICT_KEY_TO_NAME[geol]
                        for geol in geologies[idx].flatten()],
            "xvf": xvf[idx]})
    else:
        # Returns the grids as 2D array
        xvf = np.reshape(xvf, glons.shape)
        return slopes, vs30s, geologies, xvf, glons, glats, idx


def get_property_at_location(db, longitude, latitude, property_name):
    """
    Returns the property at the location from a regional raster data set
    stored in hdf5 - good for single sites and attributes, but too slow for
    large data sets
    """
    if latitude < db["latitude"][-1] or latitude > db["latitude"][0]:
        print("Latitude %.5f out of range")
        return np.nan
    if latitude < db["longitude"][0] or longitude > db["longitude"][-1]:
        print("Longitude %.5f out of range")
        return np.nan
    idx_x = np.where(db["longitude"][:] <= longitude)[0][-1]
    idx_y = np.where(db["latitude"][:] >= latitude)[0][-1]
    value = db[property_name][idx_y, idx_x]
    if value < 0:
        value = np.nan
    else:
        value = float(value)
    return value


def get_slope_geology_vs30_at_location(site_lons, site_lats, spc=(1. / 120.),
                                       onshore_only=True, as_dataframe=False):
    """
    Retrieves the slope and geology at a set of locations
    :param np.ndarray site_lons:
        Longitudes of target sites
    :param np.ndarray site_lats:
        Latitudes of target sites
    :param float spc:
        Spacing of reference hdf5 file with data (probably 120 arc seconds)
    :returns:
        site_slope: slopes of the site (in m/m)
        site_vs30: Inferred Vs30 (in m/s)
        site_geology: geological index of the site
        site_backarc_distance: backarc distance (in km)
    """
    # Get bounding box of sites (with spc buffer)
    bbox = [np.min(site_lons) - spc, np.min(site_lats) - spc,
            np.max(site_lons) + spc, np.max(site_lats) + spc]
    # Get slope and geology from hdf5 file
    print("---- Retrieving data within bounding box")
    lons, lats, slope = slice_wgs84_datafile(SITE_HDF5_FILE, bbox, "slope")
    geology = slice_wgs84_datafile(SITE_HDF5_FILE, bbox, "geology")[2]
    lons_vs, lats_vs, vs30 = slice_wgs84_datafile(VS30_HDF5_FILE, bbox, "vs30")
    if onshore_only:
        elevation = slice_wgs84_datafile(SITE_HDF5_FILE, bbox, "elevation")[2]
    else:
        elevation = np.zeros(lons.shape)
    print("---- Identifying site values")
    # Integer number of multiples of spc indicates the x and y locations
    dx = ((site_lons - (lons[0] - spc / 2.)) / spc).astype(int)
    dy = (np.fabs((site_lats - (lats[0] + spc / 2.)) / spc)).astype(int)
    dx_vs = ((site_lons - (lons_vs[0] - spc / 2.)) / spc).astype(int)
    dy_vs = (np.fabs((site_lats - (lats_vs[0] + spc / 2.)) / spc)).astype(int)
    # Get backarc distance
    print("---- Getting volcanic distance")
    xvf = interpolate_xvf_grid(site_lons, site_lats)

    # Return the slope and geology values at the selected locations
    if as_dataframe:
        print("---- Building dataframe")
        elevation = elevation[dy, dx]
        idx = elevation >= -5.0
        return gpd.GeoDataFrame({
            "geometry": gpd.GeoSeries([
                Point(lon, lat)
                for lon, lat in zip(site_lons[idx], site_lats[idx])]),
            "lon": site_lons[idx],
            "lat": site_lats[idx],
            "slope": slope[dy, dx][idx],
            "vs30": vs30[dy_vs, dx_vs][idx],
            "geology": [GEOL_DICT_KEY_TO_NAME[geol]
                        for geol in geology[dy, dx][idx]],
            "xvf": xvf[idx]})
    idx = elevation >= -5.0
    return slope[dy, dx], vs30[dy, dx], geology[dy, dx], xvf, idx


def get_slope_geology_vs30_polygons(regions, admin_id, weighting=True,
                                    method="MEAN", method_geol="MODE", n=0,
                                    proj=EUROPE_EQUAL_AREA):
    """
    For a set of polygons, retrieves the average slope, geology and vs30
    for each polygon (optionally weighted by built density) and the backarc
    distance of the centroid location.

    :param geopandas.GeoDataFrame regions:
        Administrative regions as a geopandas GeoDataFrame with geometry
        stored as a set of polygons
    :param int admin_id:
        Administrative level
    :param bool weighting:
        Apply weighting to the averaging using tbe build density
    :param str method:
        How to take the weighted average for the group of values ("MEAN",
        "MEDIAN", "MAX"), default "MEAN"
    :param str method_geol:
        How to determine preferred values from the geology data ("MODE",
        "MEDIAN", "MAX"), default "MODE"
    :param str proj:
        Projection to use for geospatial join operations (default Europe
        Lambert Equal Area
    """
    if weighting and not (method in WEIGHTING_METHODS):
        raise ValueError("Weighting method %s not recognised" % str(method))
    if method_geol not in ("MEDIAN", "MODE", "MAX"):
        raise ValueError("Geology weighting method %s not recognised"
                         % str(method_geol))
    # Extract bounding box for entire regions
    bbox = tuple(regions.total_bounds)
    print("--- Retrieving grid data")
    # Get slices of slope, geology, vs30 and built density
    lons, lats, slope = slice_wgs84_datafile(SITE_HDF5_FILE, bbox, "slope",
                                             flatten=True)
    geology = slice_wgs84_datafile(SITE_HDF5_FILE, bbox, "geology",
                                   flatten=True)[2]
    vs30 = slice_wgs84_datafile(VS30_HDF5_FILE, bbox, "vs30",
                                flatten=True)[2]

    print("--- Building and reprojecting dataframe")
    df1 = gpd.GeoDataFrame({
        "geometry": gpd.GeoSeries([Point(lon, lat)
                                   for lon, lat in zip(lons, lats)]),
        "slope": slope,
        "geology": geology,
        "vs30": vs30})
    if weighting:
        density_key = "built_density"
        if n and (n in SMOOTHED_N):
            density_key += "_n{:g}".format(n)
        density = slice_wgs84_datafile(SITE_HDF5_FILE, bbox, density_key,
                                       flatten=True)[2]
        df1["density"] = density
    df1.crs = {"init": "epsg:4326"}
    # Re-project to European (Lambert) equal area projection
    df1 = df1.to_crs({"init": EUROPE_EQUAL_AREA})
    regions_ea = regions.to_crs({"init": EUROPE_EQUAL_AREA})
    # Assign polygons to points
    print("--- Assigning sites to polygons")
    df1 = gpd.sjoin(df1, regions_ea, how="left")
    # Loop through the regions and assign the "average" properties
    vs30s = []
    slopes = []
    geologies = []
    df1_admins = df1.groupby(admin_id)
    print("--- Calculating average properties")
    for reg_id in regions[admin_id].values:
        grp = df1_admins.get_group(reg_id)
        if not (reg_id in df1_admins.groups) or not grp.shape[0]:
            print("Region %s contains no site information" % reg_id)
            # No sites within region
            vs30s.append(np.nan)
            slopes.append(np.nan)
            geologies.append(np.nan)
            continue

        if weighting:
            # Take the averages across the polygon, weighted by the density
            vs30i = WEIGHTING_METHODS[method](grp["vs30"].values,
                                              grp["density"].values)
            slopei = WEIGHTING_METHODS[method](grp["slope"].values,
                                               grp["density"].values)
        else:
            # Take the average across th polyong without density weighting
            vs30i = UNWEIGHTED_METHODS[method](grp["vs30"].values)
            slopei = UNWEIGHTED_METHODS[method](grp["slope"].values)
        vs30s.append(vs30i)
        slopes.append(slopei)
        # Get the geology
        if method_geol == "MEDIAN":
            # Get the median geology
            geologyi = WEIGHTING_METHODS["MEDIAN"](grp["geology"].values,
                                                   grp["density"].values)
        else:
            geologyi = stats.mode(grp["geology"].values).mode[0]
        geologies.append(GEOL_DICT_KEY_TO_NAME[geologyi])
    # Append this to the admin dataframe
    regions["slope"] = np.array(slopes)
    regions["vs30"] = np.array(vs30s)
    regions["geology"] = geologies
    return regions


def weighted_centroid_grid(gx, gy, spc):
    """
    Function to coarsify the high resolution (250 m x 250 m) built density
    file for Europe onto a lower resolution WGS84 defined grid
    """
    # Transform long/lat boundaries to Mollewiede projection
    # Get bounding box in terms of long, lat
    llon, llat, ulon, ulat = np.min(gx), np.min(gy), np.max(gx), np.max(gy)
    # Translate bounding box to Molleweide projection
    xlims, ylims = TRANSFORMER.transform([llon, llon, ulon, ulon],
                                         [llat, ulat, ulat, llat])
    lx, ux = np.min(xlims), np.max(xlims)
    ly, uy = np.min(ylims), np.max(ylims)
    print(lx, ux, ly, uy)
    # Load in built environment data and cut down to region constrained by
    # bounding box
    print("Retrieving data from file")
    db = h5py.File(BUILT_ENV_FILE, "r")
    xvals = db["X"][:]
    yvals = db["Y"][:]
    idx_x = np.logical_and(xvals >= lx, xvals <= ux)
    idx_y = np.logical_and(yvals >= ly, yvals <= uy)
    xvals = xvals[idx_x]
    yvals = yvals[idx_y]
    built_env = db["BUILT_ENV"][idx_y, :][:, idx_x]
    built_env[built_env < 0.0] = 0.0
    db.close()
    # Now have a 250 m resolution file of urban density, for each cell take
    # the weighted mean of all the locations in the subcells, weighted by
    # built environment proportion

    # In the Mollewiede projection, latitudes are evenly spaced, but the
    # distance between successive longitudes varies with latitude

    # Assuming that gx and gy represent the mid-points of a 'spc' spaced
    # grid, define the bounds
    print("Defining grid bounds and re-projecting")
    gxx = np.column_stack([gx - (spc / 2.), gx[:, -1] + (spc / 2.)])
    gxx = np.vstack([gxx, gxx[-1, :]])
    gyy = np.vstack([gy + (spc / 2.), gy[-1, :] - (spc / 2.)])
    gyy = np.column_stack([gyy, gyy[:, -1]])
    # Re-project the longitudes and latitudes into the Mollewiede projection
    xin, yin = TRANSFORMER.transform(gxx, gyy)
    ny, nx = gxx.shape
    ds_data = np.zeros([ny - 1, nx - 1])
    print("Coarsifying grid")
    for j in range(ny - 1):
        # Add a percentage processor
        if not (j % 100):
            perc = 100.0 * (float(j) / float(ny - 1))
            print("Computing %.1f percent" % perc, end="\r", flush=True)
        idx_y = np.logical_and(yvals <= yin[j, 0], yvals > (yin[j + 1, 0]))
        built_env_j = built_env[idx_y, :]
        for i in range(nx - 1):
            idx_x = np.logical_and(xvals >= xin[j, i],
                                   xvals < xin[j, i + 1])
            if not np.any(idx_x):
                # No cells within the range:
                continue
            # Take the mean density of the contributing cells
            ds_data[j, i] = np.mean(built_env_j[:, idx_x])
    print("\n")
    print("Done")
    return ds_data


def get_weighted_centroids_of_polygon_wgs84(admin, adm_level=1,
                                            centroid_method="MEAN", n=0):
    """
    For a set of polygons in WGS84 longitudes and latitudes, define the
    centroids at a given admin level within the dataframe. Note that in this
    case the built density has been downsampled to 30 arc seconds

    :param geopandas.GeoDataFrame admin:
        Administrative units as a geopandas Geodataframe with successive admin
        levels defined, i.e. ID_0, ID_1, ID_2, ...
    :param int adm_level:
        Admin level for aggregation
    :param str centroid_method:
        Method for defining the centroids ("MEAN", "MEDIAN", "MAX" or "").
    """
    # If the admin level is lower than that for which the geometries are
    # defined then dissolve them
    id_str = "ID_{:g}".format(adm_level)
    llon, llat, ulon, ulat = tuple(admin.total_bounds)
    print("---- Setting up admin file")
    output_admin = dissolve_admin(admin, adm_level)
    if not centroid_method:
        # If no centroid method is specified then can just take the ordinary
        # centroids of the cells
        lons, lats = zip(*[(pnt.x, pnt.y) for pnt in output_admin.centroid])
        output_admin["lon"] = np.array(lons)
        output_admin["lat"] = np.array(lats)
        return output_admin
    density_key = "built_density"
    if n and (n in SMOOTHED_N):
        density_key += "_n{:g}".format(n)
    # Now get the built density on the 30 arc-second grid
    print("---- Retrieving data from file")
    db = h5py.File(SITE_HDF5_FILE, "r")
    lons = db["longitude"][:]
    lats = db["latitude"][:]
    idx_x = np.logical_and(lons >= llon, lons <= ulon)
    idx_y = np.logical_and(lats >= llat, lats <= ulat)
    lons = lons[idx_x]
    lats = lats[idx_y]
    built_env = db[density_key][idx_y, :][:, idx_x]
    built_env[built_env < 0.0] = 0.0
    db.close()
    lons, lats = np.meshgrid(lons, lats)
    lons = lons.flatten()
    lats = lats.flatten()
    built_env = built_env.flatten()
    # Turn site data into new dataframe
    print("---- Constructing dataframe")
    site_df = gpd.GeoDataFrame({
        "geometry": gpd.GeoSeries([Point(x, y) for x, y in zip(lons, lats)]),
        "lon": lons,
        "lat": lats,
        "built": built_env})
    # Assign sites to polygons - here this is done entirely in WGS84
    # Could be done in an equal area projection if necessary - but not sure
    # it is necessary to do so
    print("---- Assigning sites to polygons")
    site_df.crs = output_admin.crs

    site_df = gpd.sjoin(site_df, output_admin, how="left")
    site_df.crs = output_admin.crs
    adm_grps = site_df.groupby("ID_{:s}".format(str(adm_level)))
    print("---- Calculating Centroids")
    weighted_centroids = []
    for i, idx in enumerate(output_admin[id_str]):
        grp = adm_grps.get_group(idx)
        # Get normalise the built area weights
        centx, centy = CENTROID_METHODS[centroid_method](
            grp["lon"].values, grp["lat"].values,  grp["built"].values)
        weighted_centroids.append([centx, centy])
    weighted_centroids = np.array(weighted_centroids)
    cent_lons = weighted_centroids[:, 0]
    cent_lats = weighted_centroids[:, 1]
    output_admin["lon"] = cent_lons
    output_admin["lat"] = cent_lats
    return output_admin


def get_weighted_centroids_of_polygon_dataframe(admin, adm_level,
                                                centroid_method="MEAN"):
    """
    Takes an input administrative shapefile as a gpd.GeoDataFrame, assigns
    the built environment cells to the corresponding admin polygons to then
    define the centroid weighted by the urban density. Unlike the WGS84 option,
    this function uses the original 250 m x 250 m built density data set
    and thus transforms the admin into the Mollewiede projection to perform
    the spatial join. It is therefore slower than the WGS84 option but
    potentially more precise and spatially correct. This may be preferable
    when considering admin zones that are potentially < 1 km in size.

    :param geopandas.GeoDataFrame:
        Administrative zones
    :param int adm_level:
        Administrative level
    :param str centroid_method:
        Method for defining the centroids of the zone ("MEAN", "MEDIAN", "MAX"
        or "")
    """
    id_str = "ID_{:s}".format(str(adm_level))
    # Admin should be in WGS84 - project to Mollewiede
    admin_moll = admin.to_crs(MOLL_STR)
    # Get total bbox
    lx, ly, ux, uy = tuple(admin_moll.total_bounds)
    # Load in the building density data from file and cut out to bounding box
    print("Retrieving data from file")
    db = h5py.File(BUILT_ENV_FILE, "r")
    xvals = db["X"][:]
    yvals = db["Y"][:]
    idx_x = np.logical_and(xvals >= lx, xvals <= ux)
    idx_y = np.logical_and(yvals >= ly, yvals <= uy)
    xvals = xvals[idx_x]
    yvals = yvals[idx_y]
    built_env = db["BUILT_ENV"][idx_y, :][:, idx_x]
    built_env[built_env < 0.0] = 0.0
    db.close()
    xvals, yvals = np.meshgrid(xvals, yvals)
    xvals = xvals.flatten()
    yvals = yvals.flatten()
    built_env = built_env.flatten()
    print("Constructing dataframe")
    # Translate into a geodataframe
    site_df = gpd.GeoDataFrame({
        "geometry": gpd.GeoSeries([Point(x, y) for x, y in zip(xvals, yvals)]),
        "xvals": xvals,
        "yvals": yvals,
        "built": built_env})
    # Assign sites to polygons
    print("Assigning sites to polygons")
    site_df.crs = admin_moll.crs
    site_df = gpd.sjoin(site_df, admin_moll, how="left")
    site_df.crs = admin_moll.crs
    adm_grps = site_df.groupby("ID_{:s}".format(str(adm_level)))
    print("Calculating Weighted centroids")
    weighted_centroids = []
    for i, idx in enumerate(admin[id_str].values):
        grp = adm_grps.get_group(idx)
        # Get normalise the built area weights
        centx, centy = CENTROID_METHODS[centroid_method](
            grp["xvals"].values, grp["yvals"].values, grp["built"].values)
        weighted_centroids.append([centx, centy])

    weighted_centroids = np.array(weighted_centroids)
    cent_lons, cent_lats = TRANSFORMER_REV.transform(weighted_centroids[:, 0],
                                                     weighted_centroids[:, 1])
    admin["centroid_lons"] = cent_lons
    admin["centroid_lats"] = cent_lats
    return admin


class SiteManager(object):
    """
    General object for controlling the flow of the site model rendering.

    For handling the output site model it is preferble to use a pandas
    dataframe, unless the density is so large that memory usage makes this
    impossible

    Site model dataframe requires the following elements:

    "lons": longitudes (decimal degrees)
    "lats": latitudes (decimal degrees)
    "slope": slope (m/s)
    "geology": geological era
    "vs30": Vs30 inferred from topography unless set otherwise
    "xvf": Distance from the volcanic front (km)
    """
    REQUIRED_HEADERS = ["lon", "lat", "slope", "geology", "vs30",
                        "vs30measured", "xvf"]
    STRING_ATTRIBS = ["geology", ]

    def __init__(self, site_model, **kwargs):
        """
        """
        assert isinstance(site_model, gpd.GeoDataFrame)
        # Store sites as a geopandas Geodataframe
        self.site_model = site_model
        site_model_crs = kwargs.get("site_model_crs", "epsg:4326")
        if not self.site_model.crs:
            self.site_model.crs = {"init": site_model_crs}
        self.bbox = self.site_model.total_bounds
        self.default_conditions = {
            "vs30": kwargs.get("default_vs30", 800.0),
            "vs30measured": kwargs.get("vs30measured", 0),
            "z1pt0": kwargs.get("default_z1pt0", 31.7),
            "z2pt5": kwargs.get("default_z2pt5", 0.57),
            "xvf": kwargs.get("xvf", 150.),
            "region": kwargs.get("region", 0)}
        # Get the ESHM regions file and re-project to xy
        self.eshm20_regions = gpd.GeoDataFrame.from_file(REGION_FILE)
        self.eshm20_regions.crs = {"init": "epsg:4326"}
        self.eshm20_regions_xy = self.eshm20_regions.to_crs(
            {"init": EUROPE_EQUAL_AREA})
        # Assign ESHM20 regions
        self.get_eshm20_region()

    def __len__(self):
        return self.site_model.shape[0]

    def __repr__(self):
        return str(self.site_model)

    def __iadd__(self, model):
        # Adds a second site model concatenating the two dataframes
        self.site_model = pd.concat([self.site_model, model.site_model])
        # Drop duplicates
        self.site_model.drop_duplicates(["lon", "lat"], inplace=True)
        return self

    def __getitem__(self, key):
        return self.site_model[key].values

    def __add__(self, model):
        output_model = pd.concat([self.site_model, model.site_model])
        output_model.drop_duplicates(["lon", "lat"], inplace=True)
        return self(output_model)

    def plot(self, key, **kwargs):
        # Plot the site model using thr GeoDataFrame.plot() method
        self.site_model.plot(key, **kwargs)

    def get_eshm20_region(self):
        """
        Determines the ESHM20 attenuation region for the respective sites and
        adds to the site model
        """
        print("Assigning the sites to the ESHM20 regions")
        # Retrieve lightweight dataframe with just the geometry and re-project
        # the cartesian reference frame
        sm_lite = self.site_model[["lat", "lon", "geometry"]].to_crs(
            {"init": EUROPE_EQUAL_AREA})
        # Get the region assignments
        assignments = gpd.sjoin(sm_lite, self.eshm20_regions_xy, how="left")
        if assignments.shape[0] > self.site_model.shape[0]:
            # This can happen if the region polygons are overlapping (which
            # they shouldn't be in this case)
            assignments.drop_duplicates(["lon", "lat"], inplace=True)
        # Set any nans to region 0
        eshm20_region = assignments["REGION"].values
        eshm20_region[np.isnan(eshm20_region)] = 0.0
        self.site_model["region"] = eshm20_region.astype(int)

    @classmethod
    def site_model_from_bbox(cls, bbox, spacing, weighting, **kwargs):
        """
        Defines a site model from a bounding box, downsampled at the given
        spacing
        """
        if (spacing % 30):
            raise ValueError("Spacing must be a multiple of 30")
        num = int(spacing) // 30
        assert bbox[2] > bbox[0]
        assert bbox[3] > bbox[1]
        averaging = kwargs.get("averaging", "MEAN")
        geol_weighting = kwargs.get("geol_weighting", "MODE")
        default_xvf = kwargs.get("default_xvf", 150.0)
        smooth_n = kwargs.get("smooth_n", 0)
        onshore_only = kwargs.get("onshore_only", True)
        as_dataframe = kwargs.get("as_dataframe", True)
        return cls(
            downsample_slope_geology_vs30_grid(bbox, num, weighting,
                                               averaging=averaging,
                                               geol_weighting=geol_weighting,
                                               default_xvf=default_xvf,
                                               n=smooth_n,
                                               onshore_only=onshore_only,
                                               as_dataframe=as_dataframe)
        )

    @classmethod
    def site_model_from_exposure_model(cls, admin_model, admin_level,
                                       weighting=True, averaging="MEAN",
                                       **kwargs):
        """
        Defines the site model from a set of polygons in the form of a
        geopandas.GeoDataFrame, taking the "average" site property across
        the entire polygon
        """
        geol_weighting = kwargs.get("geol_weighting", "MODE")
        default_xvf = kwargs.get("default_xvf", 150.0)
        smooth_n = kwargs.get("smooth_n", 0)
        # Build exposure geodataframe
        print("---- Calculating site properties")
        admin_model = get_slope_geology_vs30_polygons(
            admin_model, "ID_{:g}".format(admin_level), weighting,
            method=averaging, method_geol=geol_weighting, n=smooth_n,
            proj=EUROPE_EQUAL_AREA)
        # Get the backarc distances
        print("---- Getting backarc distance")
        admin_model["xvf"] = interpolate_xvf_grid(
            admin_model["lon"].values, admin_model["lat"].values,
            default_xvf=default_xvf)
        # Should now have a geometry (polygon), lons, lats, vs30s, geologies,
        # slopes and backarc distances - so basically a fully formed site
        # model
        return cls(admin_model, **kwargs)

    @classmethod
    def site_model_from_points(cls, lons, lats, **kwargs):
        """
        Defines the site model by assigning to each location the site property
        at the exact location of the site
        """
        onshore_only = kwargs.get("onshore_only", True)
        site_model = get_slope_geology_vs30_at_location(
            lons, lats, spc=(1. / 120.),
            onshore_only=onshore_only,
            as_dataframe=True)
        return cls(site_model, **kwargs)

    @classmethod
    def site_model_from_admin_zones(cls, admin_model, admin_level,
                                    weighting=True, centroid="MEAN",
                                    averaging="MEAN", **kwargs):
        """
        Similar to the case of the exposure model; however the starting point
        is a set of polygons without associated sites, and thus the centroids
        need to be defined.
        """
        geol_weighting = kwargs.get("geol_weighting", "MODE")
        default_xvf = kwargs.get("default_xvf", 150.0)
        smooth_n = kwargs.get("smooth_n", 0)
        print("---- Determining centroids")
        admin_model = get_weighted_centroids_of_polygon_wgs84(
            admin_model, admin_level, centroid_method=centroid, n=smooth_n)
        print("---- Getting site properties")
        admin_model = get_slope_geology_vs30_polygons(
            admin_model, "ID_{:g}".format(admin_level), weighting,
            method=averaging, method_geol=geol_weighting, n=smooth_n,
            proj=EUROPE_EQUAL_AREA)
        print("---- Getting backarc distance")
        admin_model["xvf"] = interpolate_xvf_grid(
            admin_model["lon"].values, admin_model["lat"].values,
            default_xvf=default_xvf)
        return cls(admin_model, **kwargs)

    @staticmethod
    def build_amplification_model(bbox, spacing, imts, reference_slope,
                                  reference_geology, skip_unknown=True,
                                  **kwargs):
        """
        Builds model of amplification for a given region defined by a bounding
        box

        :param list bbox:
            Bounding box to define the region [east, south, west, north]
        :param float spacing:
            Spacing of the model in arc-seconds (must be a multiple of 30)
        :param list imts:
            Intensity measure types as a list containing either "pga" or
            float values representing the periods, e.g. ["pga", 0.1, 0.2, 1.0]
        :param float reference_slope:
            Slope value (in m/m) to be adopted as the reference slope
        :param str reference_geology:
            Geological unit to be adopted as the reference geology
        :param bool skip_unknown:
            For sites with an unknown geological unit (including offshore)
            return a NaN
        **kwargs = Other arguments relevant for downsampling passed to
                   downsample_slope_geology_vs30_grid

        :returns:
            Amplification model as a dictionary containing:
            'amplification': 3D grid of amplification [nlat, nlon, num. imts]
            'phis2s': Site-to-site standard deviation (phis2s) as a list of
                      length len(imts)
            'lons': Vector of longitudes
            'lats': Vector of latitudes
            'slope': 2D array of slope values (in m/m)
            'geology': 2D numpy array of geological units
            'bbox': Bounding box information
            'spacing': spacing information
            'imts': List of intensity measured
        """
        assert bbox[2] > bbox[0]
        assert bbox[3] > bbox[1]
        if (spacing % 30):
            raise ValueError("Spacing must be a multiple of 30")
        num = int(spacing) // 30
        if num == 1:
            # Extracting the 30 arc-second model so no downsampling is needed
            lons, lats, slope = slice_wgs84_datafile(SITE_HDF5_FILE, bbox, "slope")
            geology_code = slice_wgs84_datafile(SITE_HDF5_FILE, bbox, "geology")[2]

        else:
            # Downsampling the 30 arc second grid to a lower resolution
            weighting = kwargs.get("weighting", True)
            averaging = kwargs.get("averaging", "MEAN")
            geol_weighting = kwargs.get("geol_weighting", "MODE")
            default_xvf = kwargs.get("default_xvf", 150.0)
            smooth_n = kwargs.get("smooth_n", 0)
            slope, vs30s, geology_code, xvf, lons, lats, idx =\
                downsample_slope_geology_vs30_grid(bbox, num, weighting,
                                                   averaging=averaging,
                                                   geol_weighting=geol_weighting,
                                                   default_xvf=default_xvf,
                                                   n=smooth_n,
                                                   onshore_only=False,
                                                   as_dataframe=False)
        # Convert geology from the code to the expected bytestrings
        geology = np.zeros(geology_code.shape, dtype=(np.string_, 20))
        for key in GEOL_DICT_KEY_TO_NAME:
            idx = geology_code == key
            if np.any(idx):
                geology[idx] = GEOL_DICT_KEY_TO_NAME[key]
        ampls, phis2s = site_amp_model_slope_geology(
            imts, slope, geology,
            reference_slope=reference_slope,
            reference_geology=reference_geology,
            skip_unknown=True)
        for i, (imt, phis2s_i) in enumerate(zip(imts, phis2s)):
            print("IMT: %s    PHI_S2S = %.6f" % (str(imt), phis2s_i))
        return {"amplification": ampls, "phis2s": phis2s, "lons": lons,
                "lats": lats, "slope": slope, "geology": geology,
                "bbox": bbox, "spacing": spacing, "imts": imts}

    @staticmethod
    def export_amplification_model(output_folder, model, filetype="tif"):
        """
        Exports an amplifiction model to a set of 2D raster files

        :param str output_folder:
            Path to output folder for export (will be created if it doesn't exist
        :param dict model:
            Amplification model as output from SiteManager.build_amplification_model
        :param str filetype:
            File type to export the rasters: geotiff ('tif') or ESRI arc-ascii ('asc')
        """
        spc = float(int(model["spacing"]) // 30) * (1.0 / 120.0)
        if not os.path.exists(output_folder):
            os.mkdir(output_folder)
        if filetype == "tif":
            export_amplification_maps_to_geotiff(
                output_folder,
                model["amplification"],
                model["imts"],
                model["bbox"],
                spc, spc
                )
        elif filetype == "asc":
            export_amplification_maps_to_ascii(
                output_folder,
                model["amplification"],
                model["imts"],
                model["bbox"],
                spc, spc
                )
        else:
            raise OSError("File type for export %s not recognised" % filetype)
        return

    def replace_geometry_for_lonlat(self, output_model):
        """
        Replaces the Point geometry with the longitude and latitude
        """
        lonlat = np.array([[geom.x, geom.y]
                           for geom in self.site_model.geometry.values])
        output_model["lon"] = lonlat[:, 0]
        output_model["lat"] = lonlat[:, 1]
        # Remove the original geometry (makes the output lighter)
        del output_model["geometry"]
        return output_model

    def add_backarc_distances_to_site_model(self, default_xvf=150.):
        """
        Retrieves the volcanic front distance for the target sites
        """
        if "xvf" in self.site_model.columns:
            return
        self.site_model["xvf"] =\
            interpolate_xvf_grid(
                self.site_model["lon"].values, self.site_model["lat"].values,
                default_xvf=default_xvf)

    def replace_lonlat_for_geometry(self, output_model):
        """
        Replaces the longitude and latitude with the geometry
        """
        output_model["geometry"] = gpd.GeoSeries(
           [Point(lon, lat) for lon, lat in zip(self.site_model["lon"].values,
                                                self.site_model["lat"].values)])
        # Delete the lon, lat
        del output_model["lon"]
        del output_model["lat"]
        return output_model

    def insert_default_column(self, output_model, default_key):
        """
        Adds a column of default values to the site model
        """
        default_val = self.default_conditions[default_key]
        if isinstance(default_val, str):
            # Add the list of strings
            output_model[default_key] =\
                [default_val for i in range(self.site_model.shape[0])]
        else:
            # Is numerical - add a numpy array
            output_model[default_key] =\
                default_val * np.ones(self.site_model.shape[0],
                                      dtype=default_val.__class__)

    def to_csv(self, filename, sep=","):
        """
        Exports the site model to a csv format
        """
        output_model = self.site_model.copy()
        # Check headers are correct
        invalid_points = np.zeros(output_model.shape[0], dtype=int)
        for col in self.REQUIRED_HEADERS:
            if col not in self.site_model.columns:
                if col in ("lon", "lat"):
                    # Lon/Lat not in site model - can retrieve it from geometry
                    output_model = self.replace_geometry_for_lonlat(
                        output_model)
                elif col in ("vs30measured",):
                    self.insert_default_column(output_model, "vs30measured")
                else:
                    raise ValueError("Site model missing attribute %s" % col)
            # Check for invalid values, i.e. nans
            invalid_points += output_model[col].isnull().values.astype(int)
        idx = invalid_points > 0
        if np.any(invalid_points):
            # Purge sites with nans in the required attributes
            output_model = output_model[np.logical_not(idx)]
        # Drop duplicates
        output_model.drop_duplicates(["lon", "lat"], inplace=True)
        print("Exporting model to csv file %s" % filename)
        if "geometry" in output_model.columns:
            # Remove geometry
            del output_model["geometry"]
        output_model.to_csv(filename, sep=sep, index=False)

    @classmethod
    def from_csv(cls, filename, sep=",", **kwargs):
        """
        Builds a site model object from a csv file
        """
        site_model_input = pd.read_csv(filename, sep=",")
        site_model = gpd.GeoDataFrame({
            "geometry": gpd.GeoSeries(
                [Point(lon, lat) for lon, lat in zip(site_model_input["lon"].values,
                                                     site_model_input["lat"].values)]
                ),
            }
        )

        for key in cls.REQUIRED_HEADERS:
            if key not in site_model_input.columns:
                print("Required attribute %s missing from file - may not "
                      "function as needed" % key)
            else:
                site_model[key] = site_model_input[key]
        return cls(site_model, **kwargs)

    def to_shapefile(self, filename):
        """
        Exports the site model to shapefile
        """
        output_model = self.site_model.copy()
        for col in self.REQUIRED_HEADERS:
            if col not in self.site_model.columns:
                if col in ("vs30measured",):
                    self.insert_default_column(output_model, "vs30measured")
                else:
                    raise ValueError("Site model missing attribute %s" % col)

        if "geometry" not in self.site_model.columns:
            output_model = self.replace_lonlat_for_geometry(output_model)

        print("Exporting site model to shapefile %s" % filename)
        output_model.to_file(filename)

    @classmethod
    def from_shapefile(cls, filename, **kwargs):
        """
        Loads in the site model from a shapefile
        """
        input_model = gpd.GeoDataFrame.from_file(filename)
        has_lonlat = ("lon" in input_model.columns) and\
            ("lat" in input_model.columns)
        if not has_lonlat:
            # Longitude and latitude columns missing; take them from geometry
            lons, lats = zip(*[(pnt.x, pnt.y)
                             for pnt in input_model.geometry.values])
            input_model["lon"] = np.array(lons)
            input_model["lat"] = np.array(lats)

        for key in cls.REQUIRED_HEADERS:
            if key not in input_model.columns:
                print("Required attribute %s missing from file - may not "
                      "function as needed" % key)
        return cls(input_model, **kwargs)

    def to_xml(self, filename, fmt="%s"):
        """
        Exports the site model to a site model xml file
        """
        output_model = self.site_model.copy()
        invalid_points = np.zeros(output_model.shape[0], dtype=int)
        # Check headers are correct
        for col in self.REQUIRED_HEADERS:
            if col not in self.site_model.columns:
                if col in ("lon", "lat"):
                    # Lon/Lat not in site model - can retrieve it from geometry
                    output_model = self.replace_geometry_for_lonlat(
                        output_model)
                elif col in ("vs30measured",):
                    self.insert_default_column(output_model, "vs30measured")
                else:
                    raise ValueError("Site model missing attribute %s" % col)
            # Check for invalid values, i.e. nans
            invalid_points += output_model[col].isnull().values.astype(int)
        idx = invalid_points > 0
        if np.any(invalid_points):
            # Purge sites with nans in the required attributes
            output_model = output_model[np.logical_not(idx)]
        # Drop duplicate sites
        output_model.drop_duplicates(["lon", "lat"], inplace=True)
        nodes = []
        print("Building site_model for export")
        for i in range(output_model.shape[0]):
            attribs = {"lon": output_model["lon"].values[i],
                       "lat": output_model["lat"].values[i],
                       "vs30": output_model["vs30"].values[i],
                       "slope": output_model["slope"].values[i],
                       "geology": output_model["geology"].values[i],
                       "xvf": output_model["xvf"].values[i],
                       "region": output_model["region"].values[i]}
            if output_model["vs30measured"].values[i]:
                attribs["vs30Type"] = "measured"
            else:
                attribs["vs30Type"] = "inferred"

            nodes.append(Node("site", attrib=attribs))
        site_model = Node("siteModel", nodes=nodes)
        print("Exporting site model to xml file %s" % filename)
        with open(filename, "wb") as f:
            nrml_write([site_model], f, fmt=fmt)

    @classmethod
    def from_xml(cls, filename, **kwargs):
        """
        Builds a site model object from a nrml file
        """
        [site_input] = nrml_read(filename)
        site_model = {}
        for node in site_input:
            if not site_model:
                for key in node.attrib:
                    if key in cls.STRING_ATTRIBS:
                        site_model[key] = [node[key]]
                    elif key == "vs30Type":
                        if node[key] == "measured":
                            site_model["vs30measured"] = [1]
                        else:
                            site_model["vs30measured"] = [0]

                    else:
                        site_model[key] = [float(node[key])]
            else:
                for key in node.attrib:
                    if key in cls.STRING_ATTRIBS:
                        site_model[key].append(node[key])
                    elif key == "vs30Type":
                        if node[key] == "measured":
                            site_model["vs30measured"].append(1)
                        else:
                            site_model["vs30measured"].append(0)
                    else:
                        site_model[key].append(float(node[key]))
        for key in site_model:
            if key not in cls.STRING_ATTRIBS:
                site_model[key] = np.array(site_model[key])
        site_model["geometry"] = gpd.GeoSeries(
            [Point(lon, lat) for lon, lat in zip(site_model["lon"],
                                                 site_model["lat"])]
        )
        return cls(gpd.GeoDataFrame(site_model), **kwargs)


def set_up_arg_parser():
    """
    """
    description = ("Builds the site models needed for SERA seismic risk "
                   "calculations, from the 30 arc second grids. To run type: \n"
                   "\n"
                   ">> python exposure_to_site_tools.py --run CALC_TYPE "
                   "--output-file PATH/TO/OUTPUT_FILE\n"
                   "\n"
                   "Other arguments are specific to the type of calculator\n")
    parser = argparse.ArgumentParser(
        description=description, add_help=True,
        formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('-r', '--run',
                        choices=["grid", "admin", "exposure", "point", "join",
                                 "amplification"],
                        help="Type of configuration ('grid', "
                        "'admin', 'exposure', 'point', 'join', 'amplification')",
                        required=True)
    parser.add_argument('-of', '--output-file', help='Path to output file',
                        required=True)

    # Options applicable to many groups
    parser.add_argument('-w', '--weighting', help="Use building density to weight "
                        "the values within an area/cell (default True)",
                        required=False, default="True")
    parser.add_argument('-a', '--averaging', help="Defines how to take assign a "
                        "value within a cell or polygon ('mean', 'median', max')",
                        choices=["mean", "median", "max"],
                        required=False, default="mean")
    parser.add_argument('-gw', '--geological-weighting', help="Get the weighted "
                        "median geological category (or else the modal "
                        "category)", choices=["mode", "median", "max"],
                        required=False, default="mode")
    parser.add_argument('-dxvf', '--default-xvf',
                        help="Default distance to the volcanic front  (km)",
                        required=False, default="150.0")
    parser.add_argument('-oo', '--onshore-only', help="Clip the grid sites to "
                        "only those onshore", required=False,
                        default="True")
    parser.add_argument('-sn', '--smooth-n', help="N-th order neighbourhood to"
                        " use for weighting", required=False, default="0")

    # Arguments specific to the regular grid option
    grid_group = parser.add_argument_group(
        "grid",
        "Renders site model on a regularly-spaced, weighted grid")
    grid_group.add_argument('--bbox', help="Bounding Box LLON/LLAT/ULON/ULAT",
                            required=False, default=None)
    grid_group.add_argument('-s', '--spacing', help="Spacing of downsampled grid "
                            "in arc seconds (must be a multiple of 30)",
                            required=False, default="30")

    # Arguments specific to the exposure model option
    exposure_group = parser.add_argument_group(
        "exposure",
        "Builds site model by averaging across polygons in an exposure model")
    exposure_group.add_argument('-iexp', '--input-exposure',
                                help="Path to input csv exposure file",
                                required=False, default=None)
    exposure_group.add_argument('-sd', '--shapefile-dir',
                                help="Path to directory containing shapefiles",
                                required=False, default=None)
    exposure_group.add_argument('-tal', "--target-admin-level",
                                help="Target admin level for the exposure "
                                "- highest in exposure file if not specified",
                                required=False, default=None)

    # Arguments specific to the admin regions option
    admin_group = parser.add_argument_group(
        "admin",
        "Builds a site model for a set of admin regions, including definition "
        "of the centroid")
    admin_group.add_argument('-ishp', "--input-shapefile",
                             help="Path to input admin shapefile",
                             required=False)
    admin_group.add_argument('-al', "--admin-level",
                             help="Administrative level for aggregation (will "
                             "take the highest found if not specified)",
                             required=False, default="")
    admin_group.add_argument('-cw', "--centroid-weighting",
                             help="Method for weighting the centroid of the "
                             "admin region according to building density",
                             choices=["mean", "median", "max"],
                             required=False, default="")

    # Arguments specific to the point model option
    point_group = parser.add_argument_group(
        "point",
        "Builds site model from a set of locations by taking the property "
        "value directly at the location"
        )
    point_group.add_argument('-if', "--input-file",
                             help="Path to input text/csv file",
                             required=False)

    # Arguments specific to the join model option
    join_group = parser.add_argument_group(
        "join",
        "Join together multiple site model files or a directory of files"
        )
    join_group.add_argument('-id', '--input-dir',
                            help="Path to directory of input files")
    join_group.add_argument('-f', '--files', help="+ delimited list of files "
                            "(e.g. file1.csv+file2.csv+...)")

    # Arguments specific to the amplification map option
    ampl_group = parser.add_argument_group(
        "amplification",
        "Builds a map of expected amplification for a given region "
        "(requires `bbox` and `spacing` arguments as for the `grid` workflow"
        )
    ampl_group.add_argument('-imts', '--imts', help="Comma separated list of intensity measure "
                            "types as either 'pga' or spectral period, e.g. for PGA, SA(0.1s),"
                            " SA(1.0 s) input 'pga,0.1,1.0", required=True)
    ampl_group.add_argument('-rs', '--reference-slope', help="Reference slope condition (m/m)",
                            required=True)
    ampl_group.add_argument('-rg', '--reference-geology', help="Reference geological unit "
                            "(must be one from the following: %s)" % str(GEOLOGICAL_UNITS),
                            required=True)
    ampl_group.add_argument('-ft', '--file-type', help="File type to export amplification "
                            "map: either Geotiff ('tif') or ESRI Arc-Ascii ('asc')",
                            required=False, default="tif")

    # Return arguments
    return parser


def main():
    parser = set_up_arg_parser()
    # Argparser - arguments
    args = parser.parse_args()
    # Check if output file already exists and raise error if so
    if os.path.exists(args.output_file):
        raise IOError("Output file %s already exists!" % args.output_file)

    if args.run == "amplification":
        os.mkdir(args.output_file)
    else:
        # Define output file type and check they are one of "csv", "xml" or "sha"
        output_file_type = os.path.splitext(args.output_file)[1]
        assert output_file_type in (".csv", ".xml", ".shp")

    if args.run == "grid":
        # ----------------- Workflow 1 ----------------------------------------
        # From a bounding box get a downsampled grid at a multiple of 30 arc
        # seconds
        bbox = list(map(float, args.bbox.split("/")))
        spacing = int(args.spacing)
        weighting = bool(args.weighting)
        averaging = args.averaging.upper()
        onshore_only = bool(args.onshore_only)
        geol_weighting = args.geological_weighting.upper()
        def_xvf = float(args.default_xvf)
        smooth_n = int(args.smooth_n)
        # Build the site model
        site_manager = SiteManager.site_model_from_bbox(
            bbox, spacing, weighting, averaging=averaging, geol_weighting=geol_weighting,
            default_xvf=def_xvf, onshore_only=onshore_only, smooth_n=smooth_n)

    elif args.run == "exposure":
        # ---------------- Workflow 2 -----------------------------------------
        # From an input exposure model file and a path to the corresponding
        # geometries in the shapefile, constructs a site model by averaging the
        # properties across the administrative polygon
        weighting = args.weighting
        averaging = args.averaging.upper()
        onshore_only = bool(args.onshore_only)
        geol_weighting = args.geological_weighting.upper()
        def_xvf = float(args.default_xvf)
        smooth_n = int(args.smooth_n)

        # Get the admin model (as a geodataframe) and the admin level from
        # the exposure model
        print("Retrieving exposure model from %s" % args.input_exposure)
        admin_model, admin_level = get_site_set_from_exposure(
            args.input_exposure, args.shapefile_dir,
            args.target_admin_level)
        print("Building site model")
        site_manager = SiteManager.site_model_from_exposure_model(
            admin_model, admin_level, weighting, averaging=averaging,
            geol_weighting=geol_weighting, default_xvf=def_xvf,
            smooth_n=smooth_n)

    elif args.run == "admin":
        # --------------- Workflow 3 ------------------------------------------
        # From a set of administrative boundaries defines the site model,
        # including the determination of the target sites from the [weighted]
        # centroids of each admin zone

        # Setup general properties
        weighting = args.weighting
        averaging = args.averaging.upper()
        geol_weighting = args.geological_weighting.upper()
        def_xvf = float(args.default_xvf)
        centroid_weighting = args.centroid_weighting.upper()
        smooth_n = int(args.smooth_n)
        # Admin level
        if args.admin_level:
            admin_level = int(args.admin_level)
            # Check this is in the model
        else:
            admin_level = None

        # Load in data from file
        print("Loading admin model %s" % args.input_shapefile)
        admin_model = gpd.GeoDataFrame.from_file(args.input_shapefile)
        # Just in case the ID and NAME columns are in lower case, render them
        # all to upper case
        admin_model = admin_model.rename(KEY_MAPPER, axis='columns')
        if admin_level:
            # Admin level requested but not found in shapefile
            if not "ID_{:g}".format(admin_level) in admin_model.columns:
                raise IOError("Required admin level %g not defined in "
                              "shapefile %s" % (admin_level,
                                                args.input_shapefile))
        else:
            admin_level = get_maximum_admin_level(admin_model)
            print("Highest admin level found = %g" % admin_level)
        # Now setup the site model
        print("Building site model")
        site_manager = SiteManager.site_model_from_admin_zones(
            admin_model, admin_level, weighting, centroid_weighting, averaging,
            geol_weighting=geol_weighting, default_xvf=def_xvf,
            smooth_n=smooth_n)

    elif args.run == "point":
        # --------------- Workflow 4 ------------------------------------------
        # For a set of longitudes and latitudes simply retrieve the properties
        # at the locations of the longitudes and latitudes

        onshore_only = bool(args.onshore_only)
        print("Loading locations from %s" % args.input_file)
        df = pd.read_csv(args.input_file, sep=",")
        has_lonlat = ("lon" in df.columns) and ("lat" in df.columns)
        if not has_lonlat:
            # Check if inputs in xcoord, ycoord
            has_crds = ("xcoord" in df.columns) and ("ycoord" in df.columns)
            if has_crds:
                df["lon"] = df["xcoord"].values
                df["lat"] = df["ycoord"].values
            else:
                raise ValueError("Input file %s has no lon,lat or "
                                 "xcoord,ycoord attributes" % args.input_file)
        # Dropping duplicate sites
        df.drop_duplicates(["lon", "lat"], inplace=True)
        print("Building site model for points")
        site_manager = SiteManager.site_model_from_points(
            df["lon"].values, df["lat"].values, onshore_only=onshore_only)

    elif args.run == "join":
        if args.input_dir and os.path.isdir(args.input_dir):
            files = []
            for filename in os.listdir(args.input_dir):
                if filename.endswith(".csv") or filename.endswith(".xml"):
                    files.append(os.path.join(args.input_dir, filename))
            if not len(files):
                raise ValueError("No csv or xml files found in %s"
                                 % args.input_dir)
        else:
            files = args.files.split("+")
        assert len(files) > 1

        if files[0].endswith("csv"):
            site_manager = SiteManager.from_csv(files[0])
        elif files[0].endswith("xml"):
            site_manager = SiteManager.from_xml(files[0])
        else:
            raise ValueError("File %s not csv or xml" % files[0])
        for filename in files[1:]:
            if filename.endswith("csv"):
                temp_manager = SiteManager.from_csv(filename)
            elif filename.endswith("xml"):
                temp_manager = SiteManager.from_xml(filename)
            else:
                raise ValueError("File %s not csv or xml" % filename)
            site_manager += temp_manager

    elif args.run == "amplification":
        bbox = list(map(float, args.bbox.split("/")))
        spacing = int(args.spacing)
        weighting = bool(args.weighting)
        averaging = args.averaging.upper()
        onshore_only = bool(args.onshore_only)
        geol_weighting = args.geological_weighting.upper()
        def_xvf = float(args.default_xvf)
        smooth_n = int(args.smooth_n)

        imts = [imt if imt in ("pga", "pgv") else float(imt)
                for imt in args.imts.split(",")]

        amplification = SiteManager.build_amplification_model(
            bbox, spacing, imts, float(args.reference_slope),
            args.reference_geology, skip_unknown=onshore_only, weighting=weighting,
            averaging=averaging, geol_weighting=geol_weighting,
            default_xvf=def_xvf, smooth_n=smooth_n)
        SiteManager.export_amplification_model(args.output_file,
                                               amplification,
                                               args.file_type)
        return
    else:
        # Shouldn't really have reached this point, but raising an IOError
        # just in case
        raise IOError("Run type %s not recognised" % args.run)
    print("Exporting to file %s" % args.output_file)
    # Export
    if output_file_type == ".xml":
        # Export to xml
        site_manager.to_xml(args.output_file)
    elif output_file_type == ".shp":
        site_manager.to_shapefile(args.output_file)
    else:
        # Export to csv
        site_manager.to_csv(args.output_file)
    return


if __name__ == "__main__":
    main()
