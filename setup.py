#!/usr/bin/python3

from setuptools import setup, find_packages


setup(
    name="exposure2site",
    version="1.0",
    description="Tool for building and calibrating OpenQuake site files from exposure models",
    license="GPLv3",
    extras_require = {"OQ": ['openquake.engine>=3.13',]},
    install_requires= [
    'h5py >=3.0',
    'numpy >=1.20',
    'scipy >=1.3',
    'pandas >=1.0',
    'shapely >=1.7, <2.0',
    'pyproj >=1.9',
    'rasterio >=1.2.0',
    'matplotlib',
    'geopandas',
    'ipython'],
    packages=find_packages(),
    python_requires=">=3.8",
    entry_points={"console_scripts": ["exposure2site = exposure2site.exposure_to_site_tools:main"]}
)

